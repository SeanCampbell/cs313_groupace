package decoupledGiven;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;


public class VideoView {

	// GUI
	private JFrame f = new JFrame("Video Stream");
	private JButton setupButton = new JButton("Setup");
	private JButton playButton = new JButton("Play");
	private JButton pauseButton = new JButton("Pause");
	private JButton tearButton = new JButton("Teardown");
	private JPanel mainPanel = new JPanel();
	private JPanel buttonPanel = new JPanel();
	private JLabel iconLabel = new JLabel();
	
	private ActionListener listener;

	public VideoView(){	
		// Frame
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// Buttons
		buttonPanel.setLayout(new GridLayout(1, 0));
		buttonPanel.add(setupButton);
		buttonPanel.add(playButton);
		buttonPanel.add(pauseButton);
		buttonPanel.add(tearButton);

		// Image display label
		iconLabel.setIcon(null);

		// frame layout
		mainPanel.setLayout(null);
		mainPanel.add(iconLabel);
		mainPanel.add(buttonPanel);
		iconLabel.setBounds(0, 0, 380, 280);
		buttonPanel.setBounds(0, 280, 380, 50);

		f.getContentPane().add(mainPanel, BorderLayout.CENTER);
		f.setSize(new Dimension(390, 370));
		f.setVisible(true);
		
		listener = new VideoController(this);
		addButtonListener(listener);
	}

	public void addButtonListener(ActionListener listener){
		setupButton.addActionListener(listener);
		playButton.addActionListener(listener);
		pauseButton.addActionListener(listener);
		tearButton.addActionListener(listener);
	}
}
