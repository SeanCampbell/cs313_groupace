package mainMethods;

import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import chat.server.controller.ChatController;
import chat.server.model.ChatModel;
import chat.server.model.IChatServer;

/**
 * Starts the server with port 6100
 */
public class ServerMain {
	private static final int PORT = 6100;
	private static final int TIMEOUT = 540000;

	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException | InstantiationException
				| IllegalAccessException | UnsupportedLookAndFeelException e) {
		}
		final IChatServer server = new ChatModel(ServerMain.PORT,
				ServerMain.TIMEOUT);
		@SuppressWarnings("unused")
		final ChatController ctr = new ChatController(server);
	}
}