package mainMethods;

import chat.client.controller.Controller;
import chat.client.model.ChatClient;
import chat.client.model.IChatClient;
import observer.IUpdateInfo;

/**
 * Runs the chat client and gives it a port number
 */
public class ChatClientMain {
	public static void main(String[] str){
		final int portNumber = 6100;
		IChatClient<IUpdateInfo> client = new ChatClient(portNumber);
		@SuppressWarnings("unused")
		Controller control = new Controller(client);
	}
}