package observer;

public interface IUpdateInfo {
	/**
	 * 
	 * @return the ID, which was input at the start of 
	 * the class
	 */
	int forView();
	
	/**
	 * 
	 * @return the type
	 */
	UpdateType getType();
	
	/**
	 * 
	 * @return the message
	 */
	String getMessage();
}