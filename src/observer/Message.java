package observer;

public final class Message<T> implements IMessage<T> {
	private final T message;
	
	public Message(final T message){
		this.message = message;
	}
	
	public final T getMessage(){
		return this.message;
	}
}
