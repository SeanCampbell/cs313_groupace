package observer;

public enum UpdateType {
	ERROR,
	POP_UP,
	MESSAGE,
	USERS,
	AOU,
	JOIN_LEAVE,
	ONLINE,
	OFFLINE
}