package observer;

public class ClientUpdateInfo implements IUpdateInfo{
	private int id;
	private UpdateType type;
	private String message;
	
	public ClientUpdateInfo(final int id, final UpdateType type, final String message){
		this.id = id;
		this.type = type;
		this.message = message;
	}
	
	public UpdateType getType(){
		return this.type;
	}
	
	public String getMessage(){
		return this.message;
	}

	@Override
	public int forView() {
		return this.id;
	}
}
