package observer;

/**
 * A declaration of the functionality of a message which is to be
 * transmitted from an Observable to an Observer.
 */
public interface IMessage<T> {
	/**
	 * 
	 * @return the message
	 */
	T getMessage();
}