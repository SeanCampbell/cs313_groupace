package miscellaneous;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import packets.IPacket;

public class Duplex implements IDuplex{
	private final Socket socket;
	private final Lock readLock, writeLock;
	
	public Duplex(final Socket socket){
		this.socket = socket;
		this.readLock = new ReentrantLock(true);
		this.writeLock = new ReentrantLock(true);
	}
	
	public void writePacket(IPacket<?> packet) throws IOException{
		try{
			if(this.socket != null){
				this.writeLock.lock();
				final ObjectOutputStream oos = new ObjectOutputStream(this.socket.getOutputStream());
				oos.writeObject(packet);
			}
		} finally{
			this.writeLock.unlock();
		}
	}
	
	public IPacket<?> readPacket() throws ClassNotFoundException, IOException{
		IPacket<?> ret = null;
		try{
			if(this.socket != null){
				this.readLock.lock();
				final ObjectInputStream ois = new ObjectInputStream(this.socket.getInputStream());
				ret = (IPacket<?>)ois.readObject();
			}
		} finally{
			this.readLock.unlock();
		}
		return ret;
	}
	
	public boolean readAvailable() throws ClassNotFoundException, IOException{
		boolean ret = false;
		try{
			if(this.socket != null){
				this.readLock.lock();
				ret = (socket.getInputStream().available()!=0);
			}
		} finally{
			this.readLock.unlock();
		}
		return ret;
	}

	@Override
	public String getIPAddress() {
		if(this.socket != null)
			return this.socket.getRemoteSocketAddress().toString();
		return null;
	}
	
	@Override
	public void terminate() throws IOException{
		if(this.socket != null)
			this.socket.close();
	}
}
