package miscellaneous;

import java.io.IOException;

import packets.IPacket;

public interface IDuplex {
	/**
	 * 
	 * @param packet The packet to be written to the ObjectOutputStrem
	 * @throws IOException
	 * @effects writes the packet to the socket ObjectOutputStrem
	 */
	void writePacket(IPacket<?> packet) throws IOException;
	IPacket<?> readPacket() throws ClassNotFoundException, IOException;
	
	/**
	 * 
	 * @return The read ObjectInputStrem
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	boolean readAvailable() throws ClassNotFoundException, IOException;
	String getIPAddress();
	
	/**
	 * 
	 * @throws IOException
	 * @effects closes the socket
	 */
	void terminate() throws IOException;
}