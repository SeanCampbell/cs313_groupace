package miscellaneous;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class ConcurrentList<T> implements List<T> {
	private final ReadWriteLock lock;
	private final List<T> list;
	
	public ConcurrentList(List<T> list){
		this.lock = new ReentrantReadWriteLock(true);
		this.list = list;
	}

	@Override
	public boolean add(T e) {
		this.lock.writeLock().lock();
		boolean ret = false;
		try{
			ret = this.list.add(e);
		} finally{
			this.lock.writeLock().unlock();
		}
        return ret;
	}

	@Override
	public void add(int index, T element) {
		this.lock.writeLock().lock();
		try{
			this.list.add(index, element);
		} finally{
			this.lock.writeLock().unlock();
		}
	}

	@Override
	public boolean addAll(Collection<? extends T> c) {
		this.lock.writeLock().lock();
		boolean ret = false;
		try{
			ret = this.list.addAll(c);
		} finally{
			this.lock.writeLock().unlock();
		}
		return ret;
	}

	@Override
	public boolean addAll(int index, Collection<? extends T> c) {
		boolean ret = false;
		this.lock.writeLock().lock();
		try{
			ret = this.list.addAll(index, c);
		} finally{
			this.lock.writeLock().unlock();
		}
		return ret;
	}

	@Override
	public void clear() {
		this.lock.writeLock().lock();
		try{
			this.list.clear();
		} finally{
			this.lock.writeLock().unlock();
		}
	}

	@Override
	public boolean contains(Object o) {
		this.lock.readLock().lock();
		boolean ret = false;
		try{
			ret = this.list.contains(o);
		} finally{
			this.lock.readLock().unlock();
		}
		return ret;
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		this.lock.readLock().lock();
		boolean ret = false;
		try{
			ret = this.list.containsAll(c);
		} finally{
			this.lock.readLock().unlock();
		}
		return ret;
	}

	@Override
	public T get(int index) {
		this.lock.readLock().lock();
		T ret = null;
		try{
			ret = this.list.get(index);
		} finally{
			this.lock.readLock().unlock();
		}
		return ret;
	}

	@Override
	public int indexOf(Object o) {
		this.lock.readLock().lock();
		int ret = -1;
		try{
			ret = this.list.indexOf(o);
		} finally{
			this.lock.readLock().unlock();
		}
		return ret;
	}

	@Override
	public boolean isEmpty() {
		this.lock.readLock().lock();
		boolean ret = false;
		try{
			ret = this.list.isEmpty();
		} finally{
			this.lock.readLock().unlock();
		}
		return ret;
	}

	@Override
	public Iterator<T> iterator() {
		this.lock.readLock().lock();
		Iterator<T> ret = null;
		try{
			ret = new ArrayList<>(this.list).iterator();
		} finally{
			this.lock.readLock().unlock();
		}
		return ret;
	}

	@Override
	public int lastIndexOf(Object o) {
		this.lock.readLock().lock();
		int ret = -1;
		try{
			ret = this.list.lastIndexOf(o);
		} finally{
			this.lock.readLock().unlock();
		}
		return ret;
	}

	@Override
	public ListIterator<T> listIterator() {
		this.lock.readLock().lock();
		ListIterator<T> ret = null;
		try{
			ret = new ArrayList<>(this.list).listIterator();
		} finally{
			this.lock.readLock().unlock();
		}
		return ret;
	}

	@Override
	public ListIterator<T> listIterator(int index) {
		this.lock.readLock().lock();
		ListIterator<T> ret = null;
		try{
			ret = new ArrayList<>(this.list).listIterator(index);
		} finally{
			this.lock.readLock().unlock();
		}
		return ret;
	}

	@Override
	public boolean remove(Object o) {
		this.lock.writeLock().lock();
		boolean ret = false;
		try{
			ret = this.list.remove(o);
		} finally{
			this.lock.writeLock().unlock();
		}
		return ret;
	}

	@Override
	public T remove(int index) {
		this.lock.writeLock().lock();
		T ret = null;
		try{
			ret = this.list.remove(index);
		} finally{
			this.lock.writeLock().unlock();
		}
		return ret;
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		this.lock.writeLock().lock();
		boolean ret = false;
		try{
			ret = this.list.removeAll(c);
		} finally{
			this.lock.writeLock().unlock();
		}
		return ret;
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		this.lock.writeLock().lock();
		boolean ret = false;
		try{
			ret = this.list.retainAll(c);
		} finally{
			this.lock.writeLock().unlock();
		}
		return ret;
	}

	@Override
	public T set(int index, T element) {
		this.lock.writeLock().lock();
		T ret = null;
		try{
			ret = this.list.set(index, element);
		} finally{
			this.lock.writeLock().unlock();
		}
		return ret;
	}

	@Override
	public int size() {
		this.lock.readLock().lock();
		int ret = -1;
		try{
			ret = this.list.size();
		} finally{
			this.lock.readLock().unlock();
		}
		return ret;
	}

	@Override
	public List<T> subList(int fromIndex, int toIndex) {
		this.lock.readLock().lock();
		List<T> ret = null;
		try{
			ret = this.list.subList(fromIndex, toIndex);
		} finally{
			this.lock.readLock().unlock();
		}
		return ret;
	}

	@Override
	public Object[] toArray() {
		this.lock.readLock().lock();
		Object[] ret = null;
		try{
			ret = this.list.toArray();
		} finally{
			this.lock.readLock().unlock();
		}
		return ret;
	}

	@SuppressWarnings("hiding")
	@Override
	public <T> T[] toArray(T[] a) {
		this.lock.readLock().lock();
		T[] ret = null;
		try{
			ret = this.list.toArray(a);
		} finally{
			this.lock.readLock().unlock();
		}
		return ret;
	}
	
}