package FileServer;

import java.io.*;
import java.net.*;
import java.util.List;

import packets.FilePacket;

public class Client {
	public static void main(String[] str) throws UnknownHostException,
			IOException, ClassNotFoundException {
		Socket sock = new Socket("127.0.0.1", 13267);
		System.out.println("Connecting to : " + sock);
		/*
		 * pool = Executors.newCachedThreadPool(); pool.execute(new
		 * ClientThread(sock));
		 */
		InputStream in = sock.getInputStream();
		ObjectInputStream i = new ObjectInputStream(in);
		int bytes = 0;
		FilePacket fp = (FilePacket) i.readObject();
		String name = fp.getFileName();
		System.out.println(name);
		List<Integer> buffer = fp.getMessage();
		File f = new File(System.getProperty("user.dir") + "\\Files\\" + name);
		f.createNewFile();
		FileOutputStream out = new FileOutputStream(f);
		for (bytes = 0; bytes < buffer.size(); bytes++) {
			out.write(buffer.get(bytes));
		}
		out.close();
		sock.close();
	}
}