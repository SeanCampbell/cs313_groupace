package FileServer;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

import packets.FilePacket;

public class Server {
	public static void main(String args[]) throws IOException {
		@SuppressWarnings("resource")
		ServerSocket servsock = new ServerSocket(13267);
		System.out.println("Waiting...");
		Socket sock = servsock.accept();
		while (true) {
			// while (!servsock.isClosed()) {
			//
			// //pool.execute(new ServerThread(servsock.accept(),"test.txt"));
			//
			// new Thread(new ServerThread(sock,"test.txt")).start();
			// }
			System.out.println("Accepted connection : " + sock);
			String n = "test.txt";
			String name = System.getProperty("user.dir") + "\\" + n;
			File myFile = new File(name);
			FileInputStream in = new FileInputStream(myFile);
			OutputStream out = sock.getOutputStream();
			ObjectOutputStream o = new ObjectOutputStream(out);
			int bytes = 0;
			ArrayList<Integer> buff = new ArrayList<Integer>();
			while (in.available() > 0) {
				buff.add(in.read());
				bytes++;
			}
			FilePacket fp = new FilePacket(buff, n);
			o.writeObject(fp);
			System.out.println("Transfer completed, " + bytes + " bytes sent");
			o.flush();
			out.flush();
			sock.close();
			in.close();
		}
	}
}