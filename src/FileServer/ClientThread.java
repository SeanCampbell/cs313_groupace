package FileServer;


import java.util.List;
import java.io.*;

import packets.FilePacket;

public class ClientThread implements Runnable{
	private FilePacket fp;
	
	
	/**
	 * Takes a FilePacket and strips the message bytes from it and writes them back to file.
	 * This is a set destination folder as defined with the working directory ./RFiles/name.fff
	 * */
	public ClientThread(FilePacket fp) {
		this.fp = fp;
	}

	@Override
	public void run(){
		
		try {
		String name = fp.getFileName();
		System.out.println(name);
		List<Integer> buffer = fp.getMessage();

		File f = new File(System.getProperty("user.dir")+"\\RFiles\\"+name);
		f.createNewFile();
		
		FileOutputStream out = new FileOutputStream(f);

		for (int bytes = 0; bytes<buffer.size();bytes++) {
		    out.write(buffer.get(bytes));
		   
		}


		out.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}
	
	

}
