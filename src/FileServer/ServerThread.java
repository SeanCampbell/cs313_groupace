package FileServer;


import java.util.ArrayList;
import java.io.*;

import chat.server.model.IUser;
import packets.FilePacket;

public class ServerThread implements Runnable {

	public IUser user;
	public String name;
	
	
	/**
	 * The ServerThread takes a user and a filename. The file of that name (with path resolved to
	 * (working dir) ./Files/name.fff - reads into memory as bytes in an ArrayList and then attaches 
	 * them to a packet with the message being the list of bytes. This is then transmitted using the duplex channel
	 * in the User field, defined in chat.server.model.User and miscellaneous.Duplex, back to the user that requested the file. 
	 * */
	public ServerThread(IUser user, String name) {
		this.user = user;
		this.name = name;
	}
	
	
	
	@Override
	public void run() {
		try {
	    File myFile = new File(name);
	    FileInputStream in = new FileInputStream(myFile);
	    int bytes = 0;
	    
	    ArrayList<Integer> buff = new ArrayList<Integer>();
	    while (in.available() > 0) {
	    	buff.add(in.read());
	    	bytes++;
	    }
	    String n = name.replace(myFile.getParent(),"");
	    FilePacket fp = new FilePacket(buff,n);
	    user.writePacket(fp);

	    System.out.println("Transfer completed, " + bytes + " bytes sent");
		in.close();

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
