package FileServer;

/**

 * Directory searching code adapted from 

 * http://stackoverflow.com/questions/4852531/find-files-in-a-folder-using-java

 * */
import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.List;

public class FileFinder {
	private static File DEFAULT = new File(System.getProperty("user.dir"));

	public static void main(String args[]) {
		List<File> fl;
		List<String> files = new ArrayList<String>();
		fl = getFiles(new File(System.getProperty("user.dir") + "\\Files"));
		System.out.println(getFolders(DEFAULT).toString());
		System.out.println(getFiles(new File("H:\\CS313\\boom")).toString());
		for (File f : fl) {
			files.add(new String(f.getName().replace(
					System.getProperty("user.dir") + "\\Files\\", "")));
		}
		System.out.println(files.toString());
	}

	public static List<File> getFiles(File f) {
		List<File> files = new ArrayList<File>();
		if (!f.exists())
			f = DEFAULT;
		File[] matchingFiles = f.listFiles(new FilenameFilter() {
			public boolean accept(File dir, String name) {
				return !name.startsWith(".");
			}
		});
		for (File x : matchingFiles) {
			if (!x.isHidden() && x.isFile())
				files.add(x);
		}
		return files;
	}

	public static List<File> getFolders(File f) {
		List<File> folders = new ArrayList<File>();
		if (!f.exists())
			f = DEFAULT;
		File[] matchingFiles = f.listFiles(new FilenameFilter() {
			public boolean accept(File dir, String name) {
				return !name.startsWith(".");
			}
		});
		for (File x : matchingFiles) {
			if (!x.isHidden() && x.isDirectory())
				folders.add(x);
		}
		return folders;
	}
}