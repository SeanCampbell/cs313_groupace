package chat.server.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import observer.Message;

import chat.server.model.IChatServer;
import chat.server.view.ChatView;
import chat.server.view.IChatView;

/**
 * Server side controller class which either initialises or terminates the
 * server
 */
public class ChatController implements ActionListener {
	private final IChatServer model;
	private final IChatView view;
	private ExecutorService service;

	public ChatController(final IChatServer server) {
		this.view = new ChatView();
		this.view.addButtonListener(this);
		this.model = server;
		this.model.addObserver(view);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		switch (e.getActionCommand()) {
		case "Initialise":
			if (!this.model.isRunning()) {
				this.service = Executors.newFixedThreadPool(1);
				this.service.submit(new Runnable() {
					@Override
					public void run() {
						model.run();
					}
				});
			} else {
				this.view.update(new Message<String>(
						"Server is already initialised!\n"));
			}
			break;
		case "Terminate":
			if (this.model.isRunning()) {
				this.model.shutdown();
				this.service.shutdownNow();
			} else
				this.view.update(new Message<String>(
						"Server is already shutdown!\n"));
		}
	}
}