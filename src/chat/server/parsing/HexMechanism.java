package chat.server.parsing;

// This is pretty much pulled from an apache library (the one on hex)
// namely because I would have to explicity download a full jar
// just for this single file.  I have included the copyright license because of this.
// Source: http://kickjava.com/src/org/apache/commons/codec/binary/Hex.java.htm#ixzz2zZ943qp4
/*
 * Copyright 2001-2004 The Apache Software Foundation.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class HexMechanism {
	private static final char[] VALUES = { '0', '1', '2', '3', '4', '5', '6',
			'7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };

	public static char[] encodeHex(final byte[] data) {
		final int len = data.length;
		final char[] ret = new char[len << 1];

		for (int i = 0, j = 0; i < len; i++) {
			ret[j++] = HexMechanism.VALUES[(0xF0 & data[i]) >>> 4];
			ret[j++] = HexMechanism.VALUES[0x0F & data[i]];
		}
		return ret;
	}

	public static byte[] decodeHex(final char[] data) {
		final int len = data.length, valLen = HexMechanism.VALUES.length;
		if ((len & 0x01) != 0)
			throw new UnsupportedOperationException();
		final byte[] ret = new byte[len >> 1];
		for (int i = 0, j = 0; j < len; i++) {
			int val = Character.digit(data[j], valLen) << 4;
			j++;
			val = val | Character.digit(data[j], valLen);
			j++;
			ret[i] = (byte) (val & 0xFF);
		}
		return ret;
	}

	public static void main(String[] args) {
		String value = "The";
		char[] chars = HexMechanism.encodeHex(value.getBytes());
		System.out.println(chars);
		byte[] bytes = decodeHex(chars);
		String res = new String(bytes);
		System.out.println(res);
	}
}
