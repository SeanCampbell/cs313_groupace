package chat.server.parsing;

import java.io.File;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import chat.server.model.IData;
import chat.server.model.IUser;
import chat.server.model.User;

/**
 * This class writes the data to a text file as an XML document
 */
public class DOMWriter {
	
	private static final String RELATIVE_PATH = "dataEncrypted.txt"; //File name
	
	private static final String ROOT = "DATA", ELEM = "USER", USER = "USERNAME", PASS = "PASSWORD",
			FRIEND = "FRIEND";
	
	public static void writeDataToFile(final IData data){
		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		Document doc = null;
		try {
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			// The root
			doc = docBuilder.newDocument();
			Element rootElement = doc.createElement(DOMWriter.ROOT);
			doc.appendChild(rootElement);
			
			EncryptionHandler encryption = new EncryptionHandler();
			
			Collection<IUser> users = data.getOnlineUsers(); // Collection of users
			users.addAll(data.getOfflineUsers());
			for(IUser u : users){ // For all users
				Element user = doc.createElement(DOMWriter.ELEM);
				rootElement.appendChild(user);
				
				Element userName = doc.createElement(DOMWriter.USER); // Create user element
				userName.appendChild(doc.createTextNode(u.getUserName()));
				user.appendChild(userName); // Write user name
		 
				Element pass = doc.createElement(DOMWriter.PASS); // New password element
				final String password = u.getPasswordHash();
				
				pass.appendChild(doc.createTextNode(encryption.encryptString(password)));
				user.appendChild(pass); // Write password
				
				Iterator<IUser> friends = u.getFriends(); // Get friends element
				while(friends.hasNext()){
					Element friend = doc.createElement(DOMWriter.FRIEND);
					friend.appendChild(doc.createTextNode(friends.next().getUserName()));
					user.appendChild(friend);
				}
			}
		} catch (ParserConfigurationException e1) {
		} catch (NoSuchAlgorithmException | NoSuchProviderException e) {
		} catch (InvalidKeyException | NoSuchPaddingException  | IllegalBlockSizeException  |
				BadPaddingException e) {
		}
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer;
		try {
			transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
			DOMSource source = new DOMSource(doc);
			File dir = new File(DOMWriter.RELATIVE_PATH); // Write to file
			if(dir.exists()) // If the file exist with the same name
				dir.delete(); // It is from the last run, delete it
			if(dir.createNewFile()){ // If create new file is possible
				dir.setWritable(true); // Make it write-able
				StreamResult result = new StreamResult(dir); // stream it as a file
				transformer.transform(source, result); // Transform info
				dir.setWritable(false); // Set read only
			}
		} catch (TransformerException e) {
		} catch (IOException e) {
		}
	}
	
	public static IData readDataFromFile(final IData data){
		File f = new File(DOMWriter.RELATIVE_PATH);
		if(f.exists()){ // If the file exists
			DocumentBuilderFactory fact = DocumentBuilderFactory.newInstance();
			DocumentBuilder build;
			try {
				build = fact.newDocumentBuilder();
				Document doc = build.parse(f);
				//read this - http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
				doc.getDocumentElement().normalize();
				
				EncryptionHandler decryption = new EncryptionHandler();
				
				// Amortised lut
				final Map<String, IUser> nameToUser = new HashMap<>(); // Maps names to users
				
				NodeList nList = doc.getElementsByTagName(DOMWriter.ELEM);
				for(int i = 0, len =  nList.getLength(); i < len; i++){
					Node nNode = nList.item(i);
					if(nNode.getNodeType() == Node.ELEMENT_NODE) {
						IUser temp = null;
						Element eElement = (Element) nNode;
						final String userName = eElement.getElementsByTagName(DOMWriter.USER).item(0).
								getTextContent(); // Get the user name
						final String pass = eElement.getElementsByTagName(DOMWriter.PASS).item(0).
								getTextContent(); // Get the password
						// Have we made this user already?  If so, only password and friends are needed.
						// Otherwise, create a new user ref and add it to the map.
						if(!nameToUser.containsKey(userName)){
							temp = new User(null);
							temp.setUserName(userName);
							nameToUser.put(userName, temp);
						}
						else
							temp = nameToUser.get(userName);
						temp.setPasswordHash(decryption.decryptString(pass));
						NodeList mList = eElement.getElementsByTagName(DOMWriter.FRIEND); // Get the friends
						for(int j = 0, mLen = mList.getLength(); j < mLen; j++){
							Node mNode = mList.item(j);
							if(mNode.getNodeType() == Node.ELEMENT_NODE) {
								final String friend = mNode.getTextContent();
								IUser ref = null;
								if(!nameToUser.containsKey(friend)){
									ref = new User(null);
									ref.setUserName(friend);
									nameToUser.put(friend, ref);
								}
								else
									ref = nameToUser.get(friend);
								temp.addFriend(ref);
							}
						}
					}
				}
				for(Map.Entry<String, IUser> entry : nameToUser.entrySet())
					data.addUser(entry.getValue());
			} catch (ParserConfigurationException | SAXException | IOException e) {
			} catch (NoSuchAlgorithmException | NoSuchProviderException | NoSuchPaddingException e) {
			} catch (InvalidKeyException | IllegalBlockSizeException |  BadPaddingException e) {
			}
			
		}
		return data;
	}
	
	/* If debug necessary for reading, place inside final loop to see the believed state of users.
	 * System.out.println(entry.getValue().getUserName() + "\nPass:" + entry.getValue().getPasswordHash() + 
				"\nFriends:\t");
		Iterator<IUser> x = entry.getValue().getFriends();
		while(x.hasNext())
			System.out.println(x.next().getUserName() + "\t");
		System.out.println("\n");
	 */

}