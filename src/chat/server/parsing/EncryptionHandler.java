package chat.server.parsing;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

public class EncryptionHandler {
	private static final String PUBLIC_KEY_PATH = "public_key.der";
	private static final String PRIVATE_KEY_PATH = "private_key.der";
	// Key size I used when generating via OpenSSL
	private final static int KEY_SIZE = 1024;
	
	private static final String TYPE = "RSA";
	private static final String CHARSET = "UTF-8";
	
	private final PublicKey publicKey;
	private final PrivateKey privateKey;
	private final Cipher cipher;
	
	public EncryptionHandler() throws NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException{		
		this.publicKey = this.loadPublic();
		this.privateKey = this.loadPrivate();
		this.cipher = Cipher.getInstance(EncryptionHandler.TYPE);
	}
	
	public String encryptString(final String plainText) throws InvalidKeyException, IllegalBlockSizeException, BadPaddingException{
		this.cipher.init(Cipher.ENCRYPT_MODE, this.publicKey);
		
		byte[] bytes = plainText.getBytes(Charset.forName(EncryptionHandler.CHARSET));
		byte[] encrypted = this.blockCipher(bytes, Cipher.ENCRYPT_MODE);
		
		char[] vals = HexMechanism.encodeHex(encrypted);
		return new String(vals);
	}
	
	public String decryptString(final String encrypted) throws InvalidKeyException, IllegalBlockSizeException, BadPaddingException{
		this.cipher.init(Cipher.DECRYPT_MODE, this.privateKey);
		
		byte[] bytes = HexMechanism.decodeHex(encrypted.toCharArray());
		
		byte[] decrypted = this.blockCipher(bytes, Cipher.DECRYPT_MODE);
		
		return new String(decrypted, Charset.forName(EncryptionHandler.CHARSET));
	}
	
	// Credit for the block cipher mechanism goes to Florian Westreicher 
	// http://coding.westreicher.org/?p=23
	private byte[] blockCipher(byte[] bytes, int mode) throws IllegalBlockSizeException, BadPaddingException{
		byte[] scrambled = new byte[0], ret = new byte[0];

		// General block sizes
		int length = (mode == Cipher.ENCRYPT_MODE)? (KEY_SIZE/8) - 11 : (KEY_SIZE/8);

		// Modified; item to write may be less than the actual length
		byte[] buffer = new byte[(bytes.length > length ? length : bytes.length)];

		for (int i=0; i< bytes.length; i++){
			// if we filled our buffer array we have our block ready for de- or encryption
			if ((i > 0) && (i % length == 0)){
				//execute the operation
				scrambled = cipher.doFinal(buffer);
				// add the result to our total result.
				ret = append(ret, scrambled);
				// here we calculate the length of the next buffer required
				int newlength = length;

				// if newlength would be longer than remaining bytes in the bytes array we shorten it.
				if (i + length > bytes.length) {
					 newlength = bytes.length - i;
				}
				// clean the buffer array
				buffer = new byte[newlength];
			}
			// copy byte into our buffer.
			buffer[i%length] = bytes[i];
		}

		// this step is needed if we had a trailing buffer. should only happen when encrypting.
		scrambled = cipher.doFinal(buffer);

		// final step before we can return the modified data.
		ret = append(ret, scrambled);

		return ret;
	}
	
	private byte[] append(byte[] prefix, byte[] suffix){
		byte[] toReturn = new byte[prefix.length + suffix.length];
		for (int i=0; i< prefix.length; i++){
			toReturn[i] = prefix[i];
		}
		for (int i=0; i< suffix.length; i++){
			toReturn[i+prefix.length] = suffix[i];
		}
		return toReturn;
	}
	
	private PublicKey loadPublic(){
		File f = new File(EncryptionHandler.PUBLIC_KEY_PATH);
		DataInputStream dis;
		try {
			dis = new DataInputStream(new FileInputStream(f));
			byte[] key = new byte[Math.round(f.length())];
			dis.readFully(key);
			dis.close();
			
			X509EncodedKeySpec spec = new X509EncodedKeySpec(key);
			KeyFactory kf = KeyFactory.getInstance(EncryptionHandler.TYPE);
			return kf.generatePublic(spec);
		} catch (IOException | NoSuchAlgorithmException | InvalidKeySpecException e) {
		}
		return null;
	}
	
	private PrivateKey loadPrivate(){
		File f = new File(EncryptionHandler.PRIVATE_KEY_PATH);
		DataInputStream dis;
		try {
			dis = new DataInputStream(new FileInputStream(f));
			byte[] key = new byte[Math.round(f.length())];
			dis.readFully(key);
			dis.close();
			
			PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(key);
			KeyFactory kf = KeyFactory.getInstance(EncryptionHandler.TYPE);
			return kf.generatePrivate(spec);
		} catch (IOException | NoSuchAlgorithmException | InvalidKeySpecException e) {
		}
		return null;
	}
	
}
