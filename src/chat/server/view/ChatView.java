package chat.server.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import observer.IMessage;

public class ChatView implements IChatView {
	private static final String TITLE = "Server_Controller"; // Frame title
	private static final String[] ACTIONS = {"Initialise","Execute", "Terminate"}; // Buttons
	private static final int COUNT = 3; // Buttons numbers
	
	private final JFrame frame;
	private final JPanel contentPane, buttonPanel, textCommandPanel, outputPanel;
	private final JButton[] buttons;
	
	private final JScrollPane outputScrollPane, executionScrollPane;
	private final JTextArea outputText, executionText;
	
	public ChatView(){
		this.frame = new JFrame(ChatView.TITLE);
		this.contentPane = new JPanel(false);
		this.textCommandPanel = new JPanel(false);
		this.buttonPanel = new JPanel(false);
		this.outputPanel = new JPanel(false);
		
		this.buttons = new JButton[ChatView.COUNT];
		this.outputScrollPane = new JScrollPane(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		this.executionScrollPane = new JScrollPane(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		this.outputText = new JTextArea();
		this.executionText = new JTextArea();
		
		this.setUp();
		this.frame.setVisible(true);
	}
	
	//Sets the whole view
	private void setUp(){
		this.setUpFrame();
		this.setUpContentPane();
		this.setUpOther();
	}
	
	//Sets frame
	private void setUpFrame(){
		this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.frame.setBounds(100, 100, 500, 300);
		this.frame.setContentPane(this.contentPane);
	}
	
	//Sets the content JPanel
	private void setUpContentPane(){
		this.contentPane.setBorder(BorderFactory.createMatteBorder(5, 5, 5, 5, Color.DARK_GRAY));
		this.contentPane.setLayout(new BorderLayout(0, 0));
		this.contentPane.add(this.textCommandPanel, BorderLayout.SOUTH);
		this.contentPane.add(outputPanel, BorderLayout.CENTER);
	}
	
	// Sets the text areas, j scroll panes and layouts
	private void setUpOther(){
		this.textCommandPanel.setLayout(new BorderLayout(0, 0));

		this.textCommandPanel.add(this.buttonPanel, BorderLayout.EAST);
		this.buttonPanel.setLayout(new BorderLayout(0, 0));
		this.setUpButtons();
		
		this.textCommandPanel.add(this.executionScrollPane);
		this.executionScrollPane.setViewportView(this.executionText);

		this.outputPanel.add(this.outputScrollPane);
		outputPanel.setLayout(new GridLayout(1, 1, 0, 0));
		this.outputScrollPane.setViewportView(this.outputText);	
		this.outputText.setEditable(false);
	}
	
	// Sets the buttons
	private void setUpButtons(){
		this.buttons[0] = new JButton("Initialise");
		this.buttons[0].setActionCommand(ChatView.ACTIONS[0]);
		this.buttons[0].setEnabled(true);
		this.buttonPanel.add(this.buttons[0], BorderLayout.NORTH);
		
		this.buttons[1] = new JButton("Execute");
		this.buttons[1].setActionCommand(ChatView.ACTIONS[1]);
		this.buttons[1].setEnabled(false);
		this.buttonPanel.add(this.buttons[1], BorderLayout.CENTER);
		
		this.buttons[2] = new JButton("Terminate");
		this.buttons[2].setActionCommand(ChatView.ACTIONS[2]);
		this.buttons[2].setEnabled(true);
		this.buttonPanel.add(this.buttons[2], BorderLayout.SOUTH);
	}
	
	//Updates view with the messages received
	@Override
	public void update(final IMessage<String> msg) {
		this.outputText.append(msg.getMessage());
		this.frame.repaint();
	}

	// Adds buttons listeners to all buttons
	@Override
	public void addButtonListener(ActionListener listener) {
		for(JButton button : this.buttons)
			button.addActionListener(listener);
	}
}