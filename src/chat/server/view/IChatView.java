package chat.server.view;

import observer.IObserver;

/**
 * Chat view interface which has the add button listener method to be able to
 * control that button
 */
public interface IChatView extends IObserver<String> {
	void addButtonListener(java.awt.event.ActionListener listener);
}