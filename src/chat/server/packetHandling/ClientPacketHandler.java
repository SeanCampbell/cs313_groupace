package chat.server.packetHandling;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import FileServer.ClientThread;
import chat.client.model.IChatClient;
import observer.ClientUpdateInfo;
import observer.IUpdateInfo;
import observer.Message;
import observer.UpdateType;
import packets.AckPeopleNotificationPacket;
import packets.AckNewRoomPacket;
import packets.AckPacket;
import packets.AddToRoomPacket;
import packets.AllOnlineUsersPacket;
import packets.ErrorPacket;
import packets.FileListPacket;
import packets.FilePacket;
import packets.NewMessagePacket;
import packets.OfflineNotificationPacket;
import packets.OnlineNotificationPacket;
import packets.RoomUsersPacket;
import packets.TerminateConnectionPacket;

public class ClientPacketHandler implements IClientPacketHandler {
	private final IChatClient<IUpdateInfo> client;
	
	private boolean isError;
	private int association;
	
	public ClientPacketHandler(IChatClient<IUpdateInfo> client){
		this.client = client;
		this.isError = false;
		this.association = -1;
	}
	
	@Override
	public void visitProcessPacket(AckPacket packet) {
		final String mes = this.buildMessage(packet.getMessage());
		if(!packet.canSuppress())
			this.client.notifyObservers(new Message<IUpdateInfo>(
					new ClientUpdateInfo(-1, UpdateType.POP_UP, mes)));
	}

	@Override
	public void visitProcessPacket(ErrorPacket packet) {
		this.isError = true;
		final String mes = this.buildMessage(packet.getMessage());
		this.client.notifyObservers(new Message<IUpdateInfo>(
				new ClientUpdateInfo(-1, UpdateType.ERROR, mes)));
	}
	
	/**
	 * 
	 * @param messages A list of messages that are to be added
	 * to the StringBuilder
	 * @return A string containing all the messages
	 */
	private String buildMessage(List<String> messages){
		StringBuilder builder = new StringBuilder();
		for(String s : messages)
			builder.append(s + "\n");
		return builder.toString();
	}
	

	@Override
	public void visitProcessPacket(AckNewRoomPacket packet) {
		this.association = packet.getMessage();
		final String mes = "ROOM REQUEST SUCCESS!";
		this.client.notifyObservers(new Message<IUpdateInfo>(
				new ClientUpdateInfo(-1, UpdateType.POP_UP, mes)));
	}
	
	@Override
	public void visitProcessPacket(NewMessagePacket packet) {
		this.association = packet.getRoom();
		final StringBuilder builder = new StringBuilder();
		
		final Date date = new Date(packet.getTimeStamp());
		final DateFormat formatter = new SimpleDateFormat("HH:mm:ss");
		final String dateFormatted = formatter.format(date);
		
		builder.append(dateFormatted + "  <" + packet.getSender() + ">:" +
						packet.getMessage() + "\n");
		this.client.notifyObservers(new Message<IUpdateInfo>(
				new ClientUpdateInfo(packet.getRoom(), UpdateType.MESSAGE, builder.toString())));
	}
	
	
	/**
	 * handles the file packet
	 * */
	@Override
	public void visitProcessPacket(FilePacket fpacket) {
		List<Integer> bytes = fpacket.getMessage();
		if(bytes.size() != fpacket.getExpectedLength()){
			//this.message = "UNEXPECTED LENGTH OF PACKET";
			this.isError = true;
		} else {
			//this.message = fpacket.getFileName();
			this.isError = false;
		}
		new Thread(new ClientThread(fpacket)).start();
	}
	
	
	
	
	/**
	 *Handles the fileListPacket
	 * */
	@Override
	public void visitProcessPacket(FileListPacket fileListPacket) {
		List<String> bytes = fileListPacket.getMessage();
		if(bytes.size() != fileListPacket.getExpectedLength()){
			//this.message = "UNEXPECTED LENGTH OF PACKET";
			this.isError = true;
		} else {
			//this.message = fpacket.getFileName();
			this.isError = false;
		}
	}
		
	
	
	@Override
	public void visitProcessPacket(AddToRoomPacket packet) {
		this.association = packet.getRoom();
		final String mes = packet.getMessage();
		this.client.notifyObservers(new Message<IUpdateInfo>(
				new ClientUpdateInfo(-1, UpdateType.POP_UP, mes)));
	}
	
	@Override
	public void visitProcessPacket(RoomUsersPacket packet) {
		this.association = packet.getRoom();
		final int id = packet.getRoom();
		final String mes = packet.getMessage();
		this.client.notifyObservers(new Message<IUpdateInfo>(
				new ClientUpdateInfo(id, UpdateType.USERS, mes)));
		final String name = packet.getName();
		this.client.notifyObservers(new Message<IUpdateInfo>(
			new ClientUpdateInfo(id, UpdateType.JOIN_LEAVE, name)));
	}
	
	
	
	@Override
	public void visitProcessPacket(OnlineNotificationPacket packet) {
		final String mes = packet.getMessage();
		this.client.notifyObservers(new Message<IUpdateInfo>(
			new ClientUpdateInfo(-1, UpdateType.ONLINE, mes)));
	}

	@Override
	public void visitProcessPacket(OfflineNotificationPacket packet) {
		final String mes = packet.getMessage();
		this.client.notifyObservers(new Message<IUpdateInfo>(
			new ClientUpdateInfo(-1, UpdateType.OFFLINE, mes)));
	}
	
	@Override
	public void visitProcessPacket(AckPeopleNotificationPacket packet) {
		final String online = packet.getOnline(), offline = packet.getOffline();
		this.client.notifyObservers(new Message<IUpdateInfo>(
				new ClientUpdateInfo(-1, UpdateType.ONLINE, online)));
		this.client.notifyObservers(new Message<IUpdateInfo>(
				new ClientUpdateInfo(-1, UpdateType.OFFLINE, offline)));
	}

	@Override
	public boolean wasError() {
		return this.isError;
	}

	@Override
	public int getAssociation() {
		return this.association;
	}
	
	@Override
	public void resetErrorFlag(){
		this.isError = false;
	}
	
	@Override
	public void resetAssociation(){
		this.association = -1;
	}

	@Override
	public void visitProcessPacket(TerminateConnectionPacket packet) {
		this.client.notifyObservers(new Message<IUpdateInfo>(
				new ClientUpdateInfo(-1, UpdateType.ERROR, "CONNECTION TERMINATED!")));
		System.exit(-1);
	}

	@Override
	public void visitProcessPacket(AllOnlineUsersPacket packet) {
		final String mes = packet.getMessage();
		this.client.notifyObservers(new Message<IUpdateInfo>(
			new ClientUpdateInfo(-1, UpdateType.AOU, mes)));
	}
}
