package chat.server.packetHandling;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import FileServer.FileFinder;
import FileServer.ServerThread;
import packets.AckPeopleNotificationPacket;
import packets.AckNewRoomPacket;
import packets.AckPacket;
import packets.AddFriendPacket;
import packets.AddToRoomPacket;
import packets.AllOnlineUsersPacket;
import packets.ErrorPacket;
import packets.FileListPacket;
import packets.FileRequestPacket;
import packets.PeopleNotificationPacket;
import packets.IPacket;
import packets.LeaveRoomPacket;
import packets.LogOffPacket;
import packets.LoginPacket;
import packets.NewMessagePacket;
import packets.NewRoomPacket;
import packets.OfflineNotificationPacket;
import packets.OnlineNotificationPacket;
import packets.RegisterPacket;
import packets.RemoveFriendPacket;
import packets.RoomUsersPacket;
import packets.TerminateConnectionPacket;
import chat.server.model.IChatServer;
import chat.server.model.IUser;
import chat.server.model.NoSuchUserException;
import chat.server.model.ServerMessages;

public class ServerPacketHandler implements IServerPacketHandler {
	private IChatServer server;
	private IUser sender;
	
	private IPacket<?> response;
	private boolean shouldDispose;
	
	public ServerPacketHandler(IChatServer server, IUser sender){
		this.server = server;
		this.sender = sender;
	}
	
	@Override
	public void visitProcessPacket(RegisterPacket packet) {
		final List<String> messages = (packet.isEncrypted()) ? this.decryptPacket(packet).getMessage() :
																packet.getMessage();
		
		IPacket<?> ret = null;
		if(messages.size() != packet.getExpectedLength()){
			ret = new ErrorPacket(ServerMessages.getErrorMessage("INCORRECT LENGTH OF PACKET"));
			this.shouldDispose = true;
		}
		else{
			final String name = messages.get(0), pass = messages.get(1);
			try{
				this.server.getInternalData().getUser(name);
				ret = new ErrorPacket(
						ServerMessages.getErrorMessage("USER EXISTS " + name));
				this.shouldDispose = true;
			} catch(NoSuchUserException e){
				this.sender.setUserName(name);
				this.sender.setPasswordHash(pass);
				this.server.getInternalData().addUser(this.sender);
				this.server.getInternalData().setOnline(this.sender);
				ret = new AckPacket(ServerMessages.getSuccessMessage(packet.getType().toString()));
			}
		}
		this.response = ret;
	}
	
	@Override
	public void visitProcessPacket(LoginPacket packet) {
		final List<String> messages = (packet.isEncrypted()) ? this.decryptPacket(packet).getMessage() :
			packet.getMessage();
		
		IPacket<?> ret = null;
		if(messages.size() != packet.getExpectedLength()){
			ret = new ErrorPacket(ServerMessages.getErrorMessage("INCORRECT LENGTH OF PACKET"));
			this.shouldDispose = true;
		}
		else{
			final String name = messages.get(0), pass = messages.get(1);
			try{
				IUser temp = this.server.getInternalData().getOfflineUserReference(name);
				temp.setDuplex(this.sender.getDuplex());
				this.sender = temp;
				if(!this.sender.getPasswordHash().equals(pass)){
					ret = new ErrorPacket(
							ServerMessages.getErrorMessage("INVALID PASSWORD!"));
					this.shouldDispose = true;
				}
				else{
					this.server.getInternalData().setOnline(this.sender);
					this.notifyFriends(true);
					ret = new AckPacket(ServerMessages.getSuccessMessage(packet.getType().toString()));
				}
			} catch(NoSuchUserException e){
				ret = new ErrorPacket( 
						ServerMessages.getErrorMessage("NO SUCH USER " + name));
				this.shouldDispose = true;
			}
		}
		this.response = ret;
	}
	
	@Override
	public void visitProcessPacket(PeopleNotificationPacket packet) {
		final Iterator<IUser> friends = this.sender.getFriends();
		final StringBuilder online = new StringBuilder(), offline = new StringBuilder();
		while(friends.hasNext()){
			IUser friend = friends.next();
			if(friend.isOnline())
				online.append(friend.getUserName()+"\n");
			else
				offline.append(friend.getUserName()+"\n");
		}
		
		this.response = new AckPeopleNotificationPacket(online.toString(), offline.toString());
		this.notifyAllOnline();
	}
				
	@Override
	public void visitProcessPacket(AddFriendPacket packet) {
		final String friend = packet.getMessage();
		
		IPacket<?> ret = null;
		try{
			IUser temp = this.server.getInternalData().getOnlineUserReference(friend);
			if(this.sender.getUserName().equals(temp.getUserName())){
				ret = new ErrorPacket( 
						ServerMessages.getErrorMessage("DO YOU NOT HAVE ANY FRIENDS OTHER THAN YOURSELF " + friend.toUpperCase() + "?"));
			}
			else{
				boolean already = this.sender.addFriend(temp);
				temp.addFriend(this.sender);
				if(!already)
					ret = new ErrorPacket( 
							ServerMessages.getErrorMessage("ALREADY FRIENDS WITH " + friend));
				else{
					this.sender.writePacket(new AckPacket(ServerMessages.getSuccessMessage(packet.getType().toString())));
					temp.writePacket(new AckPacket(
							"USER " + this.sender.getUserName() + " HAS ADDED YOU AS A FRIEND!"));
					ret = new OnlineNotificationPacket(temp.getUserName() + "\n");
					temp.writePacket(new OnlineNotificationPacket(this.sender.getUserName() + "\n"));
				}
			}
		} catch(NoSuchUserException e){
			ret = new ErrorPacket(
					ServerMessages.getErrorMessage("CANNOT CONTACT " + friend));
		} catch(IOException e){
			ret = new ErrorPacket( 
					ServerMessages.getErrorMessage("PROBLEM SENDING INFORMATION TO " + friend));
		}
		this.response = ret;
	}

	@Override
	public void visitProcessPacket(RemoveFriendPacket packet) {
		final String friend = packet.getMessage();
		
		IPacket<?> ret = null;
		try{
			IUser temp = this.server.getInternalData().getUser(friend);
			boolean already = this.sender.removeFriend(temp);
			temp.removeFriend(this.sender);
			if(!already)
				ret = new ErrorPacket( 
						ServerMessages.getErrorMessage("YOU ARE NOT FRIENDS WITH " + friend));
			else{
				this.sender.writePacket(new AckPacket(
						ServerMessages.getSuccessMessage(packet.getType().toString())));
				temp.writePacket(new AckPacket(
						"USER " + this.sender.getUserName() + " HAS REMOVED YOU AS A FRIEND!"));
				ret = new OfflineNotificationPacket(temp.getUserName() + "\n");
				temp.writePacket(new OfflineNotificationPacket(this.sender.getUserName() + "\n"));
			}
		} catch(NoSuchUserException e){
			ret = new ErrorPacket(
					ServerMessages.getErrorMessage("NO SUCH USER " + friend));
		} catch(IOException e){
			ret = new ErrorPacket(
					ServerMessages.getErrorMessage("PROBLEM SENDING INFORMATION TO " + friend));
		}
		this.response = ret;
	}

	@Override
	public void visitProcessPacket(NewRoomPacket packet) {	
		// No need for error handling, etc, since know type and response desired.
		final int room = this.server.getRoomHandler().addNewRoom(this.sender);
		this.response = new AckNewRoomPacket(room);
	}
	
	@Override
	public void visitProcessPacket(NewMessagePacket packet) {
		final int roomId = packet.getRoom();
		
		final IPacket<?> update = new NewMessagePacket(roomId,
				this.sender.getUserName(), packet.getMessage());
		this.server.getRoomHandler().queueMessage(roomId, update);
		final IPacket<?> ret = new AckPacket(
				ServerMessages.getSuccessMessage(packet.getType().toString()), true);
		this.response = ret;
	}
	
	@Override
	public void visitProcessPacket(AddToRoomPacket packet) {
		final int room = packet.getRoom();
		final String toAdd = packet.getMessage();
		
		IPacket<?> ret = null;
		if(this.sender.getUserName().equals(toAdd))
			ret = new ErrorPacket(
					ServerMessages.getErrorMessage("YOU CANNOT ADD YOURSELF " + toAdd + " TO THE CHAT ROOM!" ));
		else{
			try{
				IUser temp = this.server.getInternalData().getOnlineUserReference(toAdd);
				if(this.server.getRoomHandler().roomHasUser(room, temp))
					ret = new ErrorPacket(
							ServerMessages.getErrorMessage("USER " + toAdd.toUpperCase() + " IS ALREADY HERE!"));
				else{
					final IPacket<?> toSend = new AddToRoomPacket(room, 
								"USER " + this.sender.getUserName() + " INVITES YOU TO JOIN A CHAT ROOM.");
					temp.writePacket(toSend);
					ret = new AckPacket(ServerMessages.getSuccessMessage("USER " + temp.getUserName() +
								" HAS BEEN INVITED TO THE CHAT ROOM."));
					this.server.getRoomHandler().addUserToRoom(room, temp);
					final String users = this.server.getRoomHandler().getUsernames(room);
					final StringBuilder builder = new StringBuilder(temp.getUserName() + " has joined the room.\n");
					this.server.getRoomHandler().queueMessage(room, new RoomUsersPacket(room, users, 
							builder.toString()));
				}
			}catch(NoSuchUserException e){
				ret = new ErrorPacket(
						ServerMessages.getErrorMessage("CANNOT ADD " + toAdd + " TO THE CHAT ROOM!" ));
			} catch(IOException e) {
				ret = new ErrorPacket(
						ServerMessages.getErrorMessage("USER " + toAdd.toUpperCase() + " CANNOT BE CONTACTED!"));
			}
		}
		this.response = ret;
	}
	
	@Override
	public void visitProcessPacket(LeaveRoomPacket packet) {
		final int room = packet.getMessage();
		
		IPacket<?> ret = null;
		
		if(!this.server.getRoomHandler().roomHasUser(room, this.sender))
			ret = new ErrorPacket(
					ServerMessages.getErrorMessage("YOU ARE NOT IN THIS ROOM!"));
		else{
			this.server.getRoomHandler().removeUserFromRoom(room, this.sender);
			final String users = this.server.getRoomHandler().getUsernames(room);
			final StringBuilder builder = new StringBuilder(this.sender.getUserName() + " has left the room.\n");
			this.server.getRoomHandler().queueMessage(room, new RoomUsersPacket(room, users,
					builder.toString()));
			ret = new AckPacket(ServerMessages.getSuccessMessage("SUCCESSFUL DISCONNECTION FROM CHAT!"));
		}
		this.response = ret;
	}
	
	@Override
	public void visitProcessPacket(LogOffPacket packet) {
		this.server.getInternalData().setOffline(this.sender);
		this.response = new AckPacket(ServerMessages.getSuccessMessage("LOGOFF "));
		this.shouldDispose = true;
		this.notifyFriends(false);
		
	}
	
	/**
	 * 
	 * @param status A boolean holding if a user is online or offline
	 * @effects Write the user to the appropriate packet, then notify 
	 * their friends as to whether they are online or offline
	 */
	private void notifyFriends(boolean status){
		final String name = this.sender.getUserName();
		final Iterator<IUser> friends = this.sender.getFriends();
		IPacket<?> toWrite = null;
		if(status)
			toWrite = new OnlineNotificationPacket(name + "\n");
		else
			toWrite = new OfflineNotificationPacket(name + "\n");
		try{
			while(friends.hasNext()){
				final IUser ref = friends.next();
				if(ref.isOnline())
					ref.writePacket(toWrite);
			}
		} catch(IOException e){
		}
		this.notifyAllOnline();
	}
	
	/**
	 * @effects Make a list of all the online users, if a user
	 * is online notify them of all the other online users
	 */
	private void notifyAllOnline(){
		final Collection<IUser> users = this.server.getInternalData().getOnlineUsers();
		final StringBuilder builder = new StringBuilder();
		for(IUser u : users)
			builder.append(u.getUserName()+"\n");
		
		final IPacket<?> packet = new AllOnlineUsersPacket(builder.toString());
		for(IUser u : users)
			try{
				if(u.isOnline())
					u.writePacket(packet);
			} catch(IOException e){
				System.err.println("DUN GOOF");
			}
	}
	
		
	@Override
	public IPacket<?> getReturnPacket() {
		return this.response;
	}

	@Override
	public void setSender(IUser user) {
		this.sender = user;
	}

	@Override
	public void setServer(IChatServer server) {
		this.server = server;
	}
	
	private IPacket<List<String>> decryptPacket(IPacket<List<String>> packet){
		return null;
	}

	@Override
	public boolean shouldDispose() {
		return this.shouldDispose;
	}

	@Override
	public IPacket<?> sendTermination() {
		this.shouldDispose = true;
		return new TerminateConnectionPacket();
	}

	@Override
	public void visitProcessPacket(FileRequestPacket fileRequestPacket) {
		File parent = new File(System.getProperty("user.dir") + "\\Files");
		String name = fileRequestPacket.getMessage();
		if ( name == null)	{
			List<File> files = FileFinder.getFiles(parent);
			
			List<String> fl = new ArrayList<String>();
			for (File f: files) {
				fl.add(new String(f.getName().replace(System.getProperty("user.dir") + "\\Files\\", "")));
			}
			this.response = new FileListPacket(fl);
		} else
			new Thread(new ServerThread(sender,parent+"\\"+name)).start();
		
	}

	
}
