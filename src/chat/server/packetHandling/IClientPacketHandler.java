package chat.server.packetHandling;

import packets.AckPeopleNotificationPacket;
import packets.AckNewRoomPacket;
import packets.AckPacket;
import packets.AddToRoomPacket;
import packets.AllOnlineUsersPacket;
import packets.ErrorPacket;
import packets.FileListPacket;
import packets.FilePacket;
import packets.NewMessagePacket;
import packets.OfflineNotificationPacket;
import packets.OnlineNotificationPacket;
import packets.RoomUsersPacket;
import packets.TerminateConnectionPacket;

public interface IClientPacketHandler {
	
	/**
	 * 
	 * @return the current boolean state of whether this is, or
	 * isn't an error.
	 */
	boolean wasError();
	
	/**
	 * @effects set error back to false, no matter what state
	 * it's in
	 */
	void resetErrorFlag();
	
	/**
	 * 
	 * @return the association
	 */
	int getAssociation();
	
	/**
	 * @effects set association to -1
	 */
	void resetAssociation();
	
	/**
	 * @param packet The Acknowledgemnt packet
	 * @effects if the packet can't be surpressed notify
	 * the observers
	 */
	void visitProcessPacket(AckPacket packet);
	
	/**
	 * @param packet The error packet
	 * @effects set isError to true
	 * @effects notify the observers of the error
	 */
	void visitProcessPacket(ErrorPacket packet);

	/**
	 * 
	 * @param packet The acknowledgment of a new room packet
	 * @effects Set the association to the message in the packet
	 * @effects Notify the observers with "ROOM REQUEST SUCESSFUL"
	 */
	void visitProcessPacket(AckNewRoomPacket packet);
	
	/**
	 * 
	 * @param packet The new message packet
	 * @effects change the association to the room in the packet
	 * @effects notify the observers with the time stamp, sender
	 * and message
	 */
	void visitProcessPacket(NewMessagePacket packet);
	
	/**
	 * 
	 * @param packet An AddToRoomPacket
	 * @effects Set the assosiation to the room in the packet
	 * @effects Notify the observers of the users message
	 */
	void visitProcessPacket(AddToRoomPacket packet);
	
	/**
	 * 
	 * @param packet A file packet
	 * @effects create a new client thread containing the packet
	 */
	void visitProcessPacket(FilePacket packet);

	/**
	 * 
	 * @param fileListPacket A FileListPacket
	 * @effects the length of the packet message is compared
	 * to what is expected and if it's the right length isError is
	 * set to false, otherwise it's set to true
	 */
	void visitProcessPacket(FileListPacket fileListPacket);
	
	/**
	 * 
	 * @param packet A RoomUsersPacket 
	 * @effects Keeps track of the room and message
	 * and updates the client information and changes the user type to
	 * JOIN_LEAVE 
	 */
	void visitProcessPacket(RoomUsersPacket packet);

	/**
	 * 
	 * @param packet An onlineNotification packet
	 * @effects notify the observers that the client is online
	 */
	void visitProcessPacket(OnlineNotificationPacket packet);
	
	/**
	 * 
	 * @param packet An OfflineNotification packet
	 * @effects notify the observers that the client is offline
	 */
	void visitProcessPacket(OfflineNotificationPacket packet);

	/**
	 * 
	 * @param packet an AckPeopleNotificationPacket
	 * @effects notifies the observer about whether a  person is
	 * online or offline
	 */
	void visitProcessPacket(AckPeopleNotificationPacket packet);

	/**
	 * 
	 * @param packet TerminateConnectionPacket
	 * @effects notify the observers that the connection is 
	 * terminated and exit
	 */
	void visitProcessPacket(TerminateConnectionPacket packet);
	
	/**
	 * 
	 * @param packet An AllOnineUsersPacket
	 * @effects notify the observers of all the online users
	 */
	void visitProcessPacket(AllOnlineUsersPacket packet);
}
