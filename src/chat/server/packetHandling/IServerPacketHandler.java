package chat.server.packetHandling;

import packets.*;
import chat.server.model.IChatServer;
import chat.server.model.IUser;

public interface IServerPacketHandler {
	
	/**
	 * 
	 * @return the boolean shouldDispose
	 */
	boolean shouldDispose();
	
	/**
	 * 
	 * @return the response packet
	 */
	IPacket<?> getReturnPacket();

	/**
	 * 
	 * @param server The sender to be set
	 */
	void setServer(IChatServer server);
	
	/**
	 * 
	 * @param user The user to be set
	 */
	void setSender(IUser user);
	
	/**
	 * 
	 * @param fileRequestPacket a FileRequestPacket
	 * 
	 * Handles the FileRequestPackets by checking the message stored. If the message is null, 
	 * a FileListPacket is generated and set as the response packet. 
	 * 
	 * If the message is not null, a new Thread is created and the file with that name (if it exists)
	 * is transmitted to the client. A single thread is used here as it is unlikely that multiple threads 
	 * will be utilised so a threadpool is not necessary.
	 * */
	 
	void visitProcessPacket(FileRequestPacket fileRequestPacket);
	
	/**
	 * 
	 * @param packet A LoginPacket
	 * Decrypt the message, if it's encrypted. Then check to
	 * see if the username and password match. If they do 
	 * @effects set the user to online, and notify their friends
	 * otherwise
	 * @effects send an appropriate error message and set shouldDispose
	 * to true
	 */
	void visitProcessPacket(LoginPacket packet);

	/**
	 * 
	 * @param packet AddFriend packet
	 * @effects checks whether the friend to be added is already friends 
	 * with the user and online, if they are they are not already friends
	 * and online, they are added as a friend
	 * @effects otherwise an appropriate error message is sent
	 */
	void visitProcessPacket(AddFriendPacket packet);

	/**
	 * 
	 * @param packet RemoveFriend packet
	 * @effects checks whether the friend and the user are friends
	 * and if they are remove the friend. 
	 * @effects otherwise throw an appropriate error message
	 */
	void visitProcessPacket(RemoveFriendPacket packet);

	/**
	 * 
	 * @param packet A NewRoomPacket
	 * @effects Add the new room and acknowledge it
	 */
	void visitProcessPacket(NewRoomPacket packet);

	/**
	 * 
	 * @param packet A NewMessagePacket
	 * @effects The new message is created and sent to the room
	 * handler, then an acknowledgment is sent
	 */
	void visitProcessPacket(NewMessagePacket packet);

	/**
	 * 
	 * @param packet AddToRoomPacket
	 * @effects checks the use is not trying to add themselves,
	 * and that the user they are trying to add isn't already in the
	 * room. Adds the user to the chat room, or throws an appropriate
	 * error message
	 */
	void visitProcessPacket(AddToRoomPacket packet);

	/**
	 * 
	 * @param packet LeaveRoomPacket
	 * @effects Checks the user is in the room they want to leave,
	 * take the user out of the room, send a message to the other
	 * users in the room, and confirm the user has left the room.
	 * Or send an appropriate error message.
	 */
	void visitProcessPacket(LeaveRoomPacket packet);

	/**
	 * 
	 * @param packet LogOffPacket
	 * @effects Set the user to offline, and set shouldDispose
	 * to true
	 */
	void visitProcessPacket(LogOffPacket packet);

	/**
	 * 
	 * @param packet A PeopleNotificationPacket
	 * @effects for each of the users friends, check if they are
	 * online or offline, and add them to the appropriate list,
	 * then notify observers of all online users
	 */
	void visitProcessPacket(PeopleNotificationPacket packet);

	/**
	 * 
	 * @return a new TerminationConnectionPacket
	 * @effects set shouldDispose to true
	 */
	IPacket<?> sendTermination();

	/**
	 * 
	 * @param packet A RegisterPacket
	 * This decrypts the packet message, and checks if it's
	 * the correct length
	 * @effects If the message isn't the right length shouldDispose
	 * is set to true
	 * @effects Otherwise if the username already exists send an appropriate
	 * error message, and set shouldDispose to true
	 * @effects Otherwise save the user and their password and create
	 * an acknowledgment packet.
	 */
	void visitProcessPacket(RegisterPacket packet);
}