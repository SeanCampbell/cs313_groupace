package chat.server.model;

import java.util.Collection;

/**
 * Class which holds data stored in the server
 */
public interface IData {

	/**
	 * 
	 * @param userName User name of the user
	 * @return The user at the given user name
	 * @throws NoSuchUserException
	 * @requires userName != null
	 */
	IUser getUser(String userName) throws NoSuchUserException;

	/**
	 * @param userName User name of the user
	 * @return IUser online reference
	 * @throws NoSuchUserException
	 * @requires userName != null
	 */
	IUser getOnlineUserReference(String userName) throws NoSuchUserException;

	/**
	 * @param userName User name of the user
	 * @return IUser offline reference
	 * @throws NoSuchUserException
	 * @requires userName != null
	 */
	IUser getOfflineUserReference(String userName) throws NoSuchUserException;

	/**
	 * @param user User to be added
	 * @requires user != null
	 * @modifies this
	 * @effect adds user to the server
	 */
	void addUser(IUser user);

	/**
	 * @param user User to be added
	 * @requires user != null
	 * @modifies this
	 * @effect removes user to the server
	 */
	void removeUser(IUser user);

	/**
	 * @param user User which needs its status switched
	 * @requires user != null && status of the user to be off line
	 * @modifies this
	 * @effect changes the status of the user t online
	 */
	void setOnline(IUser user);

	/**
	 * @param user User which needs its status switched
	 * @requires user != null && status of the user to be on line
	 * @modifies this
	 * @effect changes the status of the user t off line
	 */
	void setOffline(IUser user);

	/**
	 * @return A collection of the online users
	 */
	Collection<IUser> getOnlineUsers();

	/**
	 * @return A collection of the offline users
	 */
	Collection<IUser> getOfflineUsers();
}