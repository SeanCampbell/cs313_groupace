package chat.server.model;

import java.io.IOException;
import java.util.List;

import packets.IPacket;

/**
 * New message thread class which is a runnable, its purpose is to write packets
 */
public class NewMessage implements Runnable {
	
	private final List<IUser> room;
	private final IPacket<?> packet;
	
	public NewMessage(final List<IUser> users, final IPacket<?> packet){
		this.room = users;
		this.packet = packet;
	}
	
	@Override
	public void run() {
		for(IUser u : this.room)
			try {
				u.writePacket(packet);
			} catch (IOException e) {
			}
	}
}
