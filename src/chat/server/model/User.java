	package chat.server.model;

import java.io.IOException;
import java.net.Socket;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicBoolean;

import miscellaneous.ConcurrentList;
import miscellaneous.Duplex;
import miscellaneous.IDuplex;

import packets.IPacket;

public class User implements IUser {
	private final AtomicBoolean isOnline;

	private IDuplex duplex;
	private String userName, passwordHash;
	// Would prefer Set
	private ConcurrentList<IUser> friends;
	
	public User(final Socket socket){
		this.isOnline = new AtomicBoolean(false);
		this.duplex = new Duplex(socket);
		//this.observers = new ConcurrentList<>(new ArrayList<IObserver<IMessage<OnOffNotification>>>());
		this.friends = new ConcurrentList<>(new ArrayList<IUser>());
	}
	
	@Override
	public final String getUserName(){
		return this.userName;
	}
	
	@Override
	public final String getPasswordHash(){
		return this.passwordHash;
	}
	
	@Override
	public final String getIPAddress(){
		return this.duplex.getIPAddress();
	}
	
	@Override
	public final void setUserName(final String userName){
		this.userName = userName;
	}
	
	@Override
	public final void setPasswordHash(final String passwordHash){
		this.passwordHash = passwordHash;
	}
	
	@Override
	public boolean addFriend(final IUser friend){
		if(!this.friends.contains(friend))
			return this.friends.add(friend);
		return false;
	}
	
	@Override
	public boolean removeFriend(final IUser friend){
		return this.friends.remove(friend);
	}
	
	@Override
	public Iterator<IUser> getFriends(){
		return this.friends.iterator();
	}
	// This constructor will block until the corresponding ObjectOutputStream has written
	// and flushed the header.
	// Therefore shouldn't need to set timeout.
	@Override
	public IPacket<?> readPacket() throws IOException, ClassNotFoundException{
		return this.duplex.readPacket();
	}

	@Override
	public void writePacket(IPacket<?> packet) throws IOException {
		this.duplex.writePacket(packet);
	}

	@Override
	public void setDuplex(IDuplex duplex) {
		this.duplex = duplex;
	}

	@Override
	public IDuplex getDuplex() {
		return this.duplex;
	}

	@Override
	public boolean isOnline() {
		return this.isOnline.get();
	}

	@Override
	public void setOnline(boolean toOnline) {
		this.isOnline.set(toOnline);
	}
}
