package chat.server.model;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import miscellaneous.ConcurrentList;

import packets.IPacket;

public class ChatRoom implements IChatRoom {
	
	private final int uid;
	// Below needed to remove its entry in the above Map.
	private final IRoomHandler handler;
	private final List<IUser> users;
	private final ExecutorService threadPool; 
	
	public ChatRoom(final int uid, final IRoomHandler handler){
		this.handler = handler;
		this.uid = uid;
		this.threadPool = Executors.newCachedThreadPool();
		this.users = new ConcurrentList<>(new ArrayList<IUser>());
	}
	
	@Override
	public void queuePacket(IPacket<?> packet){
		this.threadPool.submit(new NewMessage(new ArrayList<>(this.users), packet));
	}
	
	@Override
	public void addUser(IUser user){
		this.users.add(user);
	}
	
	@Override
	public void removeUser(IUser user){
		this.users.remove(user);
		if(this.users.isEmpty()){
			this.threadPool.shutdownNow();
			this.handler.removeRoom(this.uid);
		}
	}
	
	@Override
	public String getUsernames() {
		final ListIterator<IUser> iter = this.users.listIterator();
		final StringBuilder builder = new StringBuilder();
		while(iter.hasNext())
			builder.append(iter.next().getUserName() + "\n");
		return builder.toString();
	}
	
	@Override
	public int getRoomID(){
		return this.uid;
	}

	@Override
	public boolean hasUser(IUser user) {
		return this.users.contains(user);
	}
}
