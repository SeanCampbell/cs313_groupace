package chat.server.model;

import java.io.IOException;
import java.net.Socket;

import observer.Message;
import packets.IPacket;

import chat.server.packetHandling.IServerPacketHandler;
import chat.server.packetHandling.ServerPacketHandler;

/**
 * Runnable class to allow multi-threading
 */
public final class EstablishmentThread implements Runnable{
	//private static final int KEYSIZE = 512;
	
	private final IChatServer server;
	private final IUser user;
	
	// Don't think this should be common and shared given the return must be accessed
	// Could make it common by changing the returns of the visited to IPacket;
	// in my head this makes it slightly less clear though.
	private final IServerPacketHandler handler;
	// TODO Flesh out Crypt
	//private final Crypt crypt;
	
	public EstablishmentThread(final IChatServer server, final Socket client){
		this.server = server;
		this.user = new User(client);
		this.handler = new ServerPacketHandler(this.server, this.user);
		//this.crypt = new Crypt(KEYSIZE);
	}
	
	@Override
	public void run() {
		boolean running = true;
		while(running){
			try {
				// Fetch the input
				IPacket<?> input = this.user.readPacket();
				// Check to see if a shutdown has been attempted.
				if(!Thread.interrupted()){
					// Update any observers with receipt
					this.server.notifyObservers(new Message<String>(
							ServerMessages.notifyPacketReceipt(this.user.getIPAddress(),
									input.getType().toString())));
					// Employ visitor pattern since no multiple dispatch :-(
					input.accept(this.handler);
					IPacket<?> output = this.handler.getReturnPacket();
					// Update any observers with transmission
					this.server.notifyObservers(new Message<String>(
						ServerMessages.notifyPacketSent(this.user.getIPAddress(),
								output.getType().toString())));
					this.user.writePacket(output);
				}
				else{
					this.user.writePacket(this.handler.sendTermination());
				}
				
				if(this.handler.shouldDispose()){
					running = false;
					this.server.notifyObservers(new Message<String>(
						ServerMessages.getConnectionTerminateMessage(this.user.getIPAddress())));
					// Don't want to keep hold of the duplex info in our database
					this.user.setDuplex(null);
				}
			} catch (ClassNotFoundException | IOException e) {
				running = false;
				e.printStackTrace();
			}
		}
	}
}
