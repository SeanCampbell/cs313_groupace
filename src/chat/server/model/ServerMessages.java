package chat.server.model;

/**
 * The messages server can output to the users once an action is successful or failure
 */
public class ServerMessages {
	
	public static final String getFailStart(final int port){
		return new StringBuilder("Port" + port + " is unavailable.\n").toString();
	}
	
	public static final String getWelcome(final int port, final String ip){
		return new StringBuilder("Welcome to Server; "
				+ "we're listening to port " + port + ".\n" + "This server is running on "
						+ ip + ".\n").toString();
	}
	
	public static final String getTimeOutMessage(final int timeout){
		return new StringBuilder("No new connections have been received within "
				+ timeout + " milliseconds; thus the server is closing down.\n")
								.toString();
	}
	
	public static final String getNewThreadMessage(final String ip){
		return new StringBuilder("Connection established with  " + ip +
				".\nCreating a new thread to service this request.\n")
								.toString();
	}
	
	public static final String getCompleteFailMessage(final String trace){
		return new StringBuilder("Damn.  Something really bad happened." +
				"\nPrinting the stack trace so you know.\n" + trace)
								.toString();
	}
	
	public static final String notifyNewConnection(final String ipAddress){
		return new StringBuilder("New client connected with: " + ipAddress)
								.toString();
	}
	
	public static final String notifyPacketReceipt(final String ip, final String operation){
		return new StringBuilder(ip + " requests " + operation + "\n")
								.toString();
	}
	
	public static final String notifyPacketSent(final String ip, final String operation){
		return new StringBuilder("Server responds to " + ip + " with " + operation + "\n")
								.toString();
	}
	
	public static final String getTerminateMessage(){
		return new StringBuilder("The server has been terminated.  Good bye.\n")
			.toString();
	}
	
	public static final String getErrorMessage(final String err){
		return new StringBuilder("The server has encountered an error with the sent packet.\n" +
							err).toString();
	}
	
	public static final String getSuccessMessage(final String suc){
		return new StringBuilder("The operation of: " + suc + " was successful.\n").toString();
	}
	
	public static final String getConnectionTerminateMessage(final String ip){
		return new StringBuilder("The connection with " + ip + " has been terminated.\n").toString();
	}
}
