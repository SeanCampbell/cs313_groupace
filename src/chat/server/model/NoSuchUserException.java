package chat.server.model;

/**
 * Exception class thrown when no such user is requested/added etc
 */
public class NoSuchUserException extends Exception {

	private static final long serialVersionUID = 685691453669235201L;

	public NoSuchUserException(final String message){
		super(message);
	}
	
	public NoSuchUserException(){
		super();
	}
	
}