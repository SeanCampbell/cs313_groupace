package chat.server.model;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;

import chat.server.parsing.DOMWriter;

import observer.IMessage;
import observer.IObserver;
import observer.Message;

public class ChatModel implements IChatServer {
	private final int port, timeout;
	private final List<IObserver<String>> observers;
	private final IRoomHandler rooms;

	private static IData internalData;
	private static ServerSocket server;
	private static ExecutorService threadPool;

	private static AtomicBoolean isListening = new AtomicBoolean(false);

	public ChatModel(final int port, final int timeout) {
		this.port = port;
		this.timeout = timeout;
		this.observers = new ArrayList<>(1);
		this.rooms = new RoomHandler();
	}

	@Override
	public void run() {
		if (this.initialisedServer()) {
			this.loopServer();
			this.shutdown();
		}
	}

	/**
	 * @exception IOException
	 * @return Returns whether or not the server has been initialised
	 */
	private synchronized boolean initialisedServer() {
		boolean ret = false;
		try {
			ChatModel.server = new ServerSocket(this.port); // New server socket
															// at port
			ChatModel.server.setSoTimeout(this.timeout);
			ChatModel.threadPool = Executors.newCachedThreadPool(); // New pool
																	// of
																	// threads
																	// with no
																	// boundary
			ChatModel.internalData = DOMWriter
					.readDataFromFile(new ServerData()); // Writes the data
															// using DOM
			this.notifyObservers(new Message<String>(ServerMessages.getWelcome(
					this.port, InetAddress.getLocalHost().toString())));
			ret = true;
		} catch (IOException e) {
			this.notifyObservers(new Message<String>(ServerMessages
					.getFailStart(this.port)));
			;
		}
		return ret;
	}

	/**
	 * @exception SocketTimeoutException
	 *                , IOException
	 * @effect Listens to new connections and accepts them
	 */
	private void loopServer() {
		ChatModel.isListening.set(true);
		while (ChatModel.isListening.get()) { // Listening to new connections
			try {
				Socket clientSocket = server.accept(); // Allow access
				// Submit new runnable task
				threadPool.submit(new EstablishmentThread(this, clientSocket));
				this.notifyObservers(new Message<String>(ServerMessages
						.getNewThreadMessage(clientSocket
								.getRemoteSocketAddress().toString())));
			} catch (SocketTimeoutException e) {
				this.notifyObservers(new Message<String>(ServerMessages
						.getTimeOutMessage(this.timeout)));
				threadPool.shutdownNow(); // Shut down after timeout
				ChatModel.isListening.set(false);
			} catch (IOException e) {
				if (ChatModel.isListening.get()) {
					this.notifyObservers(new Message<String>(ServerMessages
							.getCompleteFailMessage(e.getStackTrace()
									.toString())));
				}
			}
		}
	}

	@Override
	public synchronized void shutdown() {
		ChatModel.isListening.set(false); //Stop listening
		try {
			ChatModel.server.close(); // Close the server socket
			ChatModel.threadPool.shutdownNow(); // Shut down the read pool
			DOMWriter.writeDataToFile(ChatModel.internalData); // Write the data to DOMWriter
			this.notifyObservers(new Message<String>(ServerMessages
					.getTerminateMessage()));
		} catch (IOException e) {
			this.notifyObservers(new Message<String>(ServerMessages
					.getCompleteFailMessage(e.getStackTrace().toString())));
		}
		ChatModel.server = null;
		ChatModel.threadPool = null;
	}

	@Override
	public synchronized void addObserver(final IObserver<String> obs) {
		this.observers.add(obs);
	}

	@Override
	public synchronized void removeObserver(final IObserver<String> obs) {
		this.observers.remove(obs);
	}

	@Override
	public synchronized void notifyObservers(final IMessage<String> msg) {
		for (IObserver<String> obs : this.observers)
			obs.update(msg);
	}

	@Override
	public final boolean isRunning() {
		return ChatModel.isListening.get();
	}

	@Override
	public IData getInternalData() {
		return ChatModel.internalData;
	}

	@Override
	public IRoomHandler getRoomHandler() {
		return this.rooms;
	}
}
