package chat.server.model;

import observer.IObservable;

/**
 * Chat server interface class which contains all the info required by the server
 */
public interface IChatServer extends IObservable<String> {
	
	/**
	 * Runs the server
	 */
	void run();
	
	/**
	 * @return Whether or not the server is running
	 */
	boolean isRunning();
	
	/**
	 * Shuts the server down
	 */
	void shutdown();
	
	/**
	 * @return IRoomHandler instance
	 */
	IRoomHandler getRoomHandler();
	
	/**
	 * @return Internal data stored in the server
	 */
	IData getInternalData();
}