package chat.server.model;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import packets.IPacket;

public class RoomHandler implements IRoomHandler {
	private final Map<Integer, IChatRoom> mapping;
	private int id;

	public RoomHandler() {
		this.mapping = new ConcurrentHashMap<>();
		this.id = 0;
	}

	@Override
	public int addNewRoom(IUser user) {
		IChatRoom t = new ChatRoom(this.id, this);
		t.addUser(user);
		this.mapping.put(this.id, t); // Add user to the map
		return this.id++;
	}

	@Override
	public void addUserToRoom(int room, IUser user) {
		this.mapping.get(room).addUser(user);
	}

	@Override
	public void queueMessage(int id, IPacket<?> packet) {
		if (this.mapping.containsKey(id))
			this.mapping.get(id).queuePacket(packet);
	}

	@Override
	public boolean roomHasUser(int room, IUser user) {
		if (this.mapping.containsKey(room))
			return this.mapping.get(room).hasUser(user);
		return false;
	}

	@Override
	public String getUsernames(int room) {
		if (this.mapping.containsKey(room))
			return this.mapping.get(room).getUsernames();
		return null;
	}

	@Override
	public void removeUserFromRoom(int room, IUser user) {
		this.mapping.get(room).removeUser(user);
	}

	@Override
	public void removeRoom(int room) {
		this.mapping.remove(room);
	}
}
