package chat.server.model;

import packets.IPacket;

public interface IRoomHandler {
	
	/**
	 * @param owner The user to host the new room
	 * @return the ID of the new room
	 */
	int addNewRoom(IUser owner);
	
	/**
	 * @modifies this
	 * @requires user != null
	 * @param room ID of the room
	 * @param user The user to be added to the room
	 */
	void addUserToRoom(int room, IUser user);
	
	/**
	 * @param room ID of the room
	 * @param user User to be checked
	 * @return Whether or not if the room has the user
	 */
	boolean roomHasUser(int room, IUser user);
	
	/**
	 * @param room ID of the room
	 * @return The user names of the users
	 */
	String getUsernames(int room);
	
	/**
	 * @param room ID of the room
	 * @param user The user to be removed
	 * @requires user != null
	 * @modifies this
	 */
	void removeUserFromRoom(int room, IUser user);
	
	/**
	 * @param room ID of the room
	 * @modifies this
	 * @requires Room to exist
	 * @effect The user is removed from the room
	 */
	void removeRoom(int room);
	
	/**
	 * @param room ID of the room
	 * @param packet Packet to be queued
	 * @modifies this
	 * @requires Room to exist and packet != null
	 * @effect a message is queued for packet handling
	 */
	void queueMessage(int room, IPacket<?> packet);
}
