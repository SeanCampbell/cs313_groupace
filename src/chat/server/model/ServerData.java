package chat.server.model;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class ServerData implements IData {
	// In my head, dead-lock shouldn't be possible.
	private final ReadWriteLock onlineLock, offlineLock;
	private final Set<IUser> onlineUsers, offlineUsers;

	public ServerData() {
		this.onlineLock = new ReentrantReadWriteLock(true);
		this.offlineLock = new ReentrantReadWriteLock(true);
		this.onlineUsers = new HashSet<>();
		this.offlineUsers = new HashSet<>();
	}

	@Override
	public Collection<IUser> getOnlineUsers() {
		Collection<IUser> ret = null;
		try {
			this.onlineLock.readLock().lock();
			ret = new HashSet<>(this.onlineUsers); // set of online users
		} finally {
			this.onlineLock.readLock().unlock();
		}
		return ret;
	}

	@Override
	public Collection<IUser> getOfflineUsers() {
		Collection<IUser> ret = null;
		try {
			this.offlineLock.readLock().lock();
			ret = new HashSet<>(this.offlineUsers); // set of offline users
		} finally {
			this.offlineLock.readLock().unlock();
		}
		return ret;
	}

	// In my head, it's a shorter bottle-neck to copy the entire set of users
	// than to
	// iterate across the set and do string comparisons; really depends on how
	// expensive
	// the comparison is.
	@Override
	public IUser getUser(String userName) throws NoSuchUserException {
		Set<IUser> temp = null;
		try {
			this.onlineLock.readLock().lock();
			this.offlineLock.readLock().lock();
			temp = new HashSet<>(this.onlineUsers);
			temp.addAll(this.offlineUsers);
		} finally {
			this.onlineLock.readLock().unlock();
			this.offlineLock.readLock().unlock();
		}
		return this.getUser(userName, temp);
	}

	@Override
	public IUser getOnlineUserReference(String userName)
			throws NoSuchUserException {
		Set<IUser> temp = null;
		try {
			this.onlineLock.readLock().lock();
			temp = new HashSet<>(this.onlineUsers);
		} finally {
			this.onlineLock.readLock().unlock();
		}
		return this.getUser(userName, temp);
	}

	@Override
	public IUser getOfflineUserReference(String userName)
			throws NoSuchUserException {
		Set<IUser> temp = null;
		try {
			this.offlineLock.readLock().lock();
			temp = new HashSet<>(this.offlineUsers);
		} finally {
			this.offlineLock.readLock().unlock();
		}
		return this.getUser(userName, temp);
	}

	private IUser getUser(final String needle, final Set<IUser> haystack)
			throws NoSuchUserException {
		for (IUser u : haystack)
			if (u.getUserName().equalsIgnoreCase(needle)) {
				return u;
			}
		throw new NoSuchUserException();
	}

	@Override
	public void addUser(IUser user) {
		try {
			this.offlineLock.writeLock().lock();
			this.offlineUsers.add(user);
		} finally {
			this.offlineLock.writeLock().unlock();
		}
	}

	@Override
	public void removeUser(IUser user) {
		try {
			this.offlineLock.writeLock().lock();
			this.offlineUsers.remove(user);
		} finally {
			this.offlineLock.writeLock().unlock();
		}
	}

	@Override
	public void setOnline(IUser user) {
		try {
			this.offlineLock.writeLock().lock();
			this.onlineLock.writeLock().lock();
			this.offlineUsers.remove(user);
			this.onlineUsers.add(user);
			user.setOnline(true);
		} finally {
			this.offlineLock.writeLock().unlock();
			this.onlineLock.writeLock().unlock();
		}

	}

	@Override
	public void setOffline(IUser user) {
		try {
			this.offlineLock.writeLock().lock();
			this.onlineLock.writeLock().lock();
			this.onlineUsers.remove(user);
			this.offlineUsers.add(user);
			user.setOnline(false);
		} finally {
			this.offlineLock.writeLock().unlock();
			this.onlineLock.writeLock().unlock();
		}
	}
}
