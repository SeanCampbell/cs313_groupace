package chat.server.model;

import java.io.IOException;
import java.util.Iterator;

import miscellaneous.IDuplex;

import packets.IPacket;

public interface IUser {

	/**
	 * @return Username of the user
	 */
	String getUserName();

	/**
	 * @return The password of the user
	 */
	String getPasswordHash();

	/**
	 * 
	 * @return The IP address of the user
	 */
	String getIPAddress();

	/**
	 * @param userName New user name
	 * @requires userName != null
	 * @modifies this
	 * @effect Sets the new value for userName
	 */
	void setUserName(String userName);

	/**
	 * @param passwordHash New password
	 * @requires passwordHash != null
	 * @modifies this
	 * @effect Sets the new value for passwordHash
	 */
	void setPasswordHash(String passwordHash);

	/**
	 * @param user User to be added to friends list
	 * @return True if successful else false
	 * @modifies this
	 * @effect User is added
	 */
	boolean addFriend(IUser user);

	/**
	 * @param user User to be added to friends list
	 * @return True if successful else false
	 * @modifies this
	 * @effect User is removed
	 */
	boolean removeFriend(IUser user);

	/**
	 * @return Whether or not the user was online
	 */
	boolean isOnline();

	/**
	 * @param toOnline True or false depends if we need to make the user online
	 * @modifies this
	 * @effect sets the user to online status
	 */
	void setOnline(boolean toOnline);

	/**
	 * @return An iterator through the set of friends this user has
	 */
	Iterator<IUser> getFriends();

	IDuplex getDuplex();

	void setDuplex(IDuplex duplex);

	IPacket<?> readPacket() throws IOException, ClassNotFoundException;

	void writePacket(IPacket<?> packet) throws IOException;
}