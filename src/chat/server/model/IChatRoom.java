package chat.server.model;

import packets.IPacket;

public interface IChatRoom {

	/**
	 * @param packet
	 *            The packet to be submitted as a runnable task
	 * @requires packet != null
	 * @effect adds new runnable task
	 */
	void queuePacket(IPacket<?> packet);

	/**
	 * @param user
	 *            The user to be added
	 * @requires user != null
	 * @modifies this
	 * @effect Adds user to the chat room
	 */
	void addUser(IUser user);

	/**
	 * @return A string of the user names held in the chat room
	 */
	String getUsernames();

	/**
	 * @param user
	 *            The user to be checked
	 * @return Whether the user is there or not
	 */
	boolean hasUser(IUser user);

	/**
	 * @param user
	 *            The user to be removed
	 * @requires user != null
	 * @modifies this
	 * @effect Removes user
	 */
	void removeUser(IUser user);

	/**
	 * @return The ID of THIS chat room
	 */
	int getRoomID();

}