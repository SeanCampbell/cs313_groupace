package chat.server.model;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;

/**
 * For reference:
 * http://docs.oracle.com/javase/7/docs/technotes/guides/security/
 * StandardNames.html#SecureRandom 
 * 
 * Due to time constraints, I (Sean) have been unable to get around to completing the
 * encryption aspects.  I've kept in the class as it shows at least a little research...
 */
public class Crypt {
	private static final String RNG_ALG = "SHA1PRNG";

	private final int keySize;

	private PrivateKey privKey;
	private PublicKey pubKey;

	public Crypt(final int keySize) {
		this.keySize = keySize;
		this.generateKeys();
	}

	/**
	 * Generates random keys
	 * @exception NoSuchAlgorithmException
	 */
	private void generateKeys() {
		try {
			// Strong random generator
			SecureRandom rng = SecureRandom.getInstance(RNG_ALG);
			KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
			keyGen.initialize(this.keySize, rng); // Initialise the key pair generator
			KeyPair pair = keyGen.generateKeyPair(); // Generate key pair
			this.privKey = pair.getPrivate();
			this.pubKey = pair.getPublic();
		} catch (NoSuchAlgorithmException e) {
		}
	}
	
	public PrivateKey getPrivate(){
		return this.privKey;
	}
	
	public PublicKey getPublic(){
		return this.pubKey;
	}
}
