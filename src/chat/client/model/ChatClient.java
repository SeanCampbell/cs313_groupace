package chat.client.model;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import miscellaneous.Duplex;
import miscellaneous.IDuplex;
import chat.server.packetHandling.ClientPacketHandler;
import chat.server.packetHandling.IClientPacketHandler;
import observer.ClientUpdateInfo;
import observer.IMessage;
import observer.IObserver;
import observer.IUpdateInfo;
import observer.Message;
import observer.UpdateType;
import packets.AddFriendPacket;
import packets.AddToRoomPacket;
import packets.FileRequestPacket;
import packets.PeopleNotificationPacket;
import packets.IPacket;
import packets.LeaveRoomPacket;
import packets.LogOffPacket;
import packets.LoginPacket;
import packets.NewMessagePacket;
import packets.NewRoomPacket;
import packets.RegisterPacket;
import packets.RemoveFriendPacket;

public class ChatClient implements IChatClient<IUpdateInfo> {
	private final List<IObserver<IUpdateInfo>> observers;
	
	private final int port;
	private final IClientPacketHandler handler;

	private final AtomicBoolean waiting;
	
	private IDuplex duplex;
	private String host;
	
	public ChatClient(final int port){
		this.observers = new ArrayList<>();
		this.port = port;
		this.handler = new ClientPacketHandler(this);
		this.waiting = new AtomicBoolean(false);
	}
	
	public boolean connectToServer(final String host){
		boolean ret = false;
		try{
			//final InetAddress addr = InetAddress.getByName(host);
			this.host = host;
			Socket connection = new Socket(host, this.port); // Connects to server with this port and address
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
			}
			this.duplex = new Duplex(connection);
		} catch(UnknownHostException e){
			this.notifyObservers(new Message<IUpdateInfo>(
					new ClientUpdateInfo(-1,UpdateType.ERROR, "UNKNOWN HOST!")));
		} catch(IOException e){
			this.notifyObservers(new Message<IUpdateInfo>(
					new ClientUpdateInfo(-1,UpdateType.ERROR, "UNABLE TO  CONNECT TO HOST!")));
		}
		return ret;
	}

	// Simple connection establishment with reasonably detailed error handling.
	@Override
	public boolean connectToServer(final String host, final String userName,
			final String password, final boolean toRegister, final boolean encrypt) {
		final String concat = userName + "/n" + password;
		boolean ret = false;
		try{
			//final InetAddress addr = InetAddress.getByName(host);
			this.host = host;
			Socket connection = new Socket(host, this.port); // Connects to server with this port and address
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
			}
			this.duplex = new Duplex(connection);
			ret = this.simpleOp(-1, (toRegister) ? new RegisterPacket(false, concat) :
										new LoginPacket(false, concat));
		} catch(UnknownHostException e){
			this.notifyObservers(new Message<IUpdateInfo>(
					new ClientUpdateInfo(-1,UpdateType.ERROR, "UNKNOWN HOST!")));
		} catch(IOException e){
			this.notifyObservers(new Message<IUpdateInfo>(
					new ClientUpdateInfo(-1,UpdateType.ERROR, "UNABLE TO  CONNECT TO HOST!")));
		} catch (ClassNotFoundException e) {
			this.notifyObservers(new Message<IUpdateInfo>(
					new ClientUpdateInfo(-1,UpdateType.ERROR, "SERVER SENT AN UNRECOGNISED PACKET!")));
		}
		return ret;
	}
	
	/**
	 * @param obs An observer to be added
	 * @requires abs != null
	 * @effect Adds observer
	 */
	@Override
	public synchronized void addObserver(IObserver<IUpdateInfo> obs) {
		this.observers.add(obs);
	}

	/**
	 * @param obs An observer to be removed
	 * @requires abs != null
	 * @effect Removes observer
	 */
	@Override
	public synchronized void removeObserver(IObserver<IUpdateInfo> obs) {
		this.observers.remove(obs);
	}

	/**
	 * Updates observers
	 */
	@Override
	public synchronized void notifyObservers(IMessage<IUpdateInfo> msg) {
		for(IObserver<IUpdateInfo> obs : this.observers)
			obs.update(msg);
	}

	@Override
	public void addFriend(String friendName) throws IOException, ClassNotFoundException {
		this.simpleOp(-1, new AddFriendPacket(friendName));
	}
	
	@Override
	public void removeFriend(String friendName) throws IOException, ClassNotFoundException {
		this.simpleOp(-1, new RemoveFriendPacket(friendName));
	}
	
	private boolean simpleOp(int updateNo, IPacket<?> packet) throws IOException, ClassNotFoundException{
		boolean ret = true;
		this.duplex(packet);
		if(this.handler.wasError()){
			ret = false;
			this.handler.resetErrorFlag();
		}
		return ret;
	}
	
	private void duplex(IPacket<?> packet) throws IOException, ClassNotFoundException{
		// Check for anything to read; if there is, update accordingly.
		this.duplex.writePacket(packet);
		IPacket<?> rec = this.duplex.readPacket();
		rec.accept(this.handler);
	}
	
	@Override
	public void requestInitialUpdate() throws IOException,
			ClassNotFoundException {
		IPacket<?> packet = new PeopleNotificationPacket();
		this.simpleOp(-1, packet);
	}

	@Override
	public int requestNewChatRoom() throws IOException, ClassNotFoundException {
		IPacket<?> packet = new NewRoomPacket();
		// Do the simple operation and then return the room number.  Error will be shown to the user via observer
		// so just throw an exception for the controller to handle.
		if(this.simpleOp(-1, packet)){
			int ret = this.handler.getAssociation();
			this.handler.resetAssociation();
			return ret;
		}
		throw new IOException();
	}

	@Override
	public void sendChatMessage(final int room, final String message) throws IOException, ClassNotFoundException {
		IPacket<?> packet = new NewMessagePacket(room, null, message);
		this.simpleOp(room, packet);
	}

	@Override
	public void addToRoom(int room, String name) throws IOException,
			ClassNotFoundException {
		IPacket<?> packet = new AddToRoomPacket(room, name);
		this.simpleOp(room, packet);
	}
	
	@Override
	public boolean leaveRoom(int room) throws IOException, ClassNotFoundException {
		IPacket<?> packet = new LeaveRoomPacket(room);
		return this.simpleOp(room, packet);
	}


	@Override
	public boolean logOff() throws IOException, ClassNotFoundException {
		IPacket<?> packet = new LogOffPacket();
		return this.simpleOp(-1, packet);
	}
	
	public int update() throws IOException, ClassNotFoundException{
		// Updates have to be explicitly called via a timer.  The controller return
		// is if someone else wants to add this client to a chat-room.
		if(this.duplex.readAvailable()){
			IPacket<?> packet = this.duplex.readPacket();
			this.waiting.set(this.duplex.readAvailable());
			packet.accept(this.handler);
			return this.handler.getAssociation();
		}
		return -1;
	}
	
	@Override
	public boolean arePacketsWaiting() {
		return this.waiting.get();
	}
	
	@Override
	public void disconnect() throws IOException{
		this.duplex.terminate();
		this.duplex = null;
		this.waiting.set(false);
	}

	
	/**
	 * requestFileTransfer(String n) performs two operations depending on the state of the parameter.
	 * If the filename is set, the server will request the file of that name and send it - providing it exists.
	 * if the Filename is null then the server will send a list of files to choose from in it's file directory.
	 * 
	 * 
	 * @param n
	 * 		The filename being requested.
	 * */
	@SuppressWarnings("unchecked")
	@Override
	public List<String> requestFileTransfer(String n) throws IOException, ClassNotFoundException {
		
		IPacket<?> packet = new FileRequestPacket(n);
		this.duplex.writePacket(packet);
		packet = this.duplex.readPacket();
		packet.accept(this.handler);
		if (this.handler.wasError()) return null;		
		if(packet.getMessage() instanceof List) return (List<String>) packet.getMessage();
		else return null;
		
	}

	@Override
	public int getPort() {
		return this.port;
	}

	@Override
	public String getHost() {
		return this.host;
	}
}
