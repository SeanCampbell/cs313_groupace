package chat.client.model;

import java.io.IOException;
import java.util.List;

import observer.IObservable;

/**
 * 
 * An Observable client interface which is used to identify client and hide its
 * implementations, it contains all the method a typical client-server chat
 * server has from the client side
 * 
 * @param <T> Generic class
 */
public interface IChatClient<T> extends IObservable<T> {
	
	boolean connectToServer(String ipAddress);
	
	boolean connectToServer(String ipAddress, String userName, String password,
			boolean toRegister, boolean encrypt);

	/**
	 * @effect Updates the friends with the newest notifications
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	void requestInitialUpdate() throws IOException, ClassNotFoundException;

	/**
	 * @param friendName The name of the friend to be added
	 * @throws IOException
	 * @throws ClassNotFoundException
	 * @requires friendName != null
	 * @modifies this
	 * @effect Adds a friend to the set of friends
	 */
	void addFriend(String friendName) throws IOException,
			ClassNotFoundException;

	/**
	 * @param friendName The name of the friend to be removed
	 * @throws IOException
	 * @throws ClassNotFoundException
	 * @requires friendName != null
	 * @modifies this
	 * @effect removes friend from the friends set
	 */
	void removeFriend(String friendName) throws IOException,
			ClassNotFoundException;

	/**
	 * @return The new chat room
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	int requestNewChatRoom() throws IOException, ClassNotFoundException;

	/**
	 * 
	 * @param room ID of the room
	 * @param message The message to be send
	 * @throws IOException
	 * @throws ClassNotFoundException
	 * @requires message != null
	 * @modifies this
	 * @effect Sends a message to the server
	 */
	void sendChatMessage(int room, String message) throws IOException,
			ClassNotFoundException;

	/**
	 * @param room ID of the room
	 * @param name Name of the room
	 * @throws IOException
	 * @throws ClassNotFoundException
	 * @requires name != null
	 * @modifies this
	 * @effect Adds client to the room
	 */
	void addToRoom(int room, String name) throws IOException,
			ClassNotFoundException;

	/**
	 * @param room ID of the room
	 * @return True if the leave room is successful else false
	 * @throws IOException
	 * @throws ClassNotFoundException
	 * @effect Client leaves the room
	 */
	boolean leaveRoom(int room) throws IOException, ClassNotFoundException;

	/**
	 * 
	 * @return Whether or not a client was logged off
	 * @throws IOException
	 * @throws ClassNotFoundException
	 * @effect Logs the client off the server
	 */
	boolean logOff() throws IOException, ClassNotFoundException;

	/**
	 * @return Whether or not the packets are in a waiting states
	 */
	boolean arePacketsWaiting();

	/**
	 * @modifies this
	 * @effect disconnects a client from the server
	 * @throws IOException
	 */
	void disconnect() throws IOException;

	/**
	 * @return The update ID
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	int update() throws IOException, ClassNotFoundException;
	
	int getPort();
	
	String getHost();
	
	List<String> requestFileTransfer(String n) throws IOException, ClassNotFoundException;
}