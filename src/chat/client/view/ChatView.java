package chat.client.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import observer.IMessage;
import observer.IObserver;
import observer.IUpdateInfo;
import observer.UpdateType;

/**
 * 
 * Observer class which basically is the chat view, frame which displays content
 * such as online friends, buttons for available actions and output text
 */
public class ChatView implements IObserver<IUpdateInfo> {
	private final JFrame frame;
	private final JTextArea outputText, inputText, clientsText;
	private final JScrollPane output, input, clients;
	private final JPanel mainPanel, buttonsPanel, inputPanel, outputPanel,
			clientsPanel;
	private final JButton send, add, remove;

	private final int id;
	
	public ChatView(final int id) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException | InstantiationException
				| IllegalAccessException | UnsupportedLookAndFeelException e) {
		}
		frame = new JFrame("Chat Window");
		mainPanel = new JPanel();
		inputPanel = new JPanel();
		buttonsPanel = new JPanel();
		outputPanel = new JPanel();
		clientsPanel = new JPanel();
		clientsText = new JTextArea();
		send = new JButton("Send");
		remove = new JButton("Leave");
		add = new JButton("Add");
		clients = new JScrollPane(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		output = new JScrollPane(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		input = new JScrollPane(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		outputText = new JTextArea();
		inputText = new JTextArea();
		setPanels();
		setTextAreas();
		setFrame();

		this.id = id;
	}
	
	public static void main(String[] args) {
		new ChatView(0);
	}

	/**
	 * Sets the frame and its values
	 */
	private void setFrame() {
		frame.setVisible(true);
		frame.setBounds(50, 50, 700, 400);
		frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		frame.setContentPane(mainPanel);
	}

	/**
	 * Sets the panel to hold each item of the chat view
	 */
	private void setPanels() {
		mainPanel.setLayout(new BorderLayout(0, 0));
		mainPanel.add(inputPanel, BorderLayout.SOUTH);
		mainPanel.add(outputPanel, BorderLayout.CENTER);

		inputPanel.setLayout(new BorderLayout(0, 0));
		inputPanel.add(buttonsPanel, BorderLayout.EAST);

		buttonsPanel.setLayout(new BorderLayout(0, 0));
		buttonsPanel.add(send, BorderLayout.NORTH);
		buttonsPanel.add(add, BorderLayout.CENTER);
		buttonsPanel.add(remove, BorderLayout.SOUTH);

		clientsPanel.setLayout(new BorderLayout(0, 0));
		clientsText.append(" Who's Online \n");
		mainPanel.add(clientsPanel, BorderLayout.EAST);
	}

	/**
	 * Sets the text areas and some "look and feel" parameters
	 */
	private void setTextAreas() {
		inputPanel.add(input);
		input.setViewportView(inputText);
		inputText.setBackground(Color.BLACK);
		inputText.setForeground(Color.CYAN);

		outputPanel.add(output);
		outputPanel.setLayout(new GridLayout(1, 1, 0, 0));
		output.setViewportView(outputText);
		outputText.setEditable(false);
		outputText.setBackground(Color.BLACK);
		outputText.setForeground(Color.CYAN);

		clientsPanel.add(clients);
		clientsPanel.setLayout(new GridLayout(1, 1, 0, 0));
		clients.setViewportView(clientsText);
		clientsText.setEditable(false);
		clientsText.setBackground(Color.BLACK);
		clientsText.setForeground(Color.GREEN);
	}

	/**
	 * Updates the user with the current info depending on a chosen action
	 */
	@Override
	public void update(final IMessage<IUpdateInfo> message) {
		final IUpdateInfo info = message.getMessage();
		if (info.forView() == this.id) {
			final int ord = info.getType().ordinal();
			final String m = info.getMessage();
			if (ord == UpdateType.MESSAGE.ordinal())
				this.outputText.append(m);
			else if (ord == UpdateType.USERS.ordinal())
				this.clientsText.setText(m);
			else if (ord == UpdateType.JOIN_LEAVE.ordinal())
				this.outputText.append(m);
			else if (ord == UpdateType.ERROR.ordinal())
				JOptionPane.showMessageDialog(frame, m, "ERROR",
						JOptionPane.ERROR_MESSAGE);
			else if (ord == UpdateType.POP_UP.ordinal())
				JOptionPane.showMessageDialog(frame, m, "INFO",
						JOptionPane.INFORMATION_MESSAGE);
		}
	}

	/**
	 * 
	 * @param listener
	 *            Listener used to register a listener for each button
	 */
	public void addButtonListener(ActionListener listener) {
		send.addActionListener(listener);
		add.addActionListener(listener);
		remove.addActionListener(listener);
	}

	public String showEntryDialog(final String title) {
		return JOptionPane.showInputDialog(this.frame, title);
	}

	/**
	 * 
	 * @param message
	 *            Confirmation message, returns true or false depending on the
	 *            input
	 * @return
	 */
	public boolean showConfirmDialog(final String message) {
		return JOptionPane.showConfirmDialog(this.frame, "Please confirm...",
				message, JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION;
	}

	/**
	 * Clears the input after the user have wrote something
	 */
	public void clearInputText() {
		inputText.setText("");
	}

	/**
	 * Disposes the current frame
	 */
	public void dispose() {
		this.frame.dispose();
	}

	/**
	 * @return The text written by the user
	 */
	public String getInput() {
		return inputText.getText();
	}

	/**
	 * @return The ID of the user
	 */
	public final int getID() {
		return this.id;
	}

	/**
	 * @param message
	 *            To be passed to the JOpionPane
	 */
	public void showErrorDialog(final String message) {
		JOptionPane.showMessageDialog(this.frame, message, "ERROR",
				JOptionPane.ERROR_MESSAGE);
	}

}