package chat.client.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.util.HashSet;
import java.util.Set;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;
import observer.IMessage;
import observer.IObserver;
import observer.IUpdateInfo;
import observer.UpdateType;

/**
 * An observer class which has the initial view after the user logs on, it
 * should basically display to the user which actions can be chosen
 */
public class MainView implements IObserver<IUpdateInfo> {

	private final JFrame frame;
	private final JPanel mainPanel, buttonPanel, onlinePanel, offlinePanel,
			othersPanel;
	private final JButton newChat, addFriend, removeFriend,// streamVideo,
			shareFile, logOut;
	private final JScrollPane online, offline, others;
	private final JTextArea onlineA, offlineA, othersT;

	private final Set<String> onlineX, offlineX, othersX;

	public MainView() {
		addFriend = new JButton("Add Friend");
		othersPanel = new JPanel();
		newChat = new JButton("New Chat Room");
		mainPanel = new JPanel();
		frame = new JFrame("Choose An Action");
		removeFriend = new JButton("Remove Friend");
		//streamVideo = new JButton("Stream Video");
		shareFile = new JButton("Share File");
		logOut = new JButton("Log Out");
		online = new JScrollPane(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		offline = new JScrollPane(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		others = new JScrollPane(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		buttonPanel = new JPanel(new GridLayout(5, 3, 6, 3));
		onlinePanel = new JPanel();
		offlinePanel = new JPanel();
		onlineA = new JTextArea();
		offlineA = new JTextArea();
		othersT = new JTextArea();
		setTextAreas();
		setPanels();
		setFrame();

		this.onlineX = new HashSet<>();
		this.offlineX = new HashSet<>();
		this.othersX = new HashSet<>();
	}

	/**
	 * Sets the text areas and combines them with the JScrollPanes
	 */
	private void setTextAreas() {
		onlinePanel.add(online);
		onlinePanel.setLayout(new GridLayout(1, 1, 0, 0));
		online.setViewportView(onlineA);
		onlineA.setEditable(false);

		offlinePanel.add(offline);
		offlinePanel.setLayout(new GridLayout(1, 1, 0, 0));
		offline.setViewportView(offlineA);
		offlineA.setEditable(false);

		othersPanel.add(others);
		othersPanel.setLayout(new GridLayout(1, 1, 0, 0));
		others.setViewportView(othersT);
		othersT.setEditable(false);

		onlineA.append(" Online Friends \n ");
		offlineA.append(" Offline Friends \n");
		othersT.append(" All Users \n ");
		onlineA.setBackground(Color.BLACK);
		offlineA.setBackground(Color.BLACK);
		othersT.setBackground(Color.BLACK);
		onlineA.setForeground(Color.GREEN);
		offlineA.setForeground(Color.RED);
		othersT.setForeground(Color.WHITE);
	}

	/**
	 * Sets the panels that divide each section of the GUI, set positions etc
	 */
	private void setPanels() {
		mainPanel.setLayout(new BorderLayout(0, 0));
		mainPanel.add(buttonPanel, BorderLayout.WEST);
		mainPanel.add(onlinePanel, BorderLayout.CENTER);
		mainPanel.add(offlinePanel, BorderLayout.EAST);
		offlinePanel.add(othersPanel, BorderLayout.EAST);
	}

	/**
	 * Sets the frame
	 */
	private void setFrame() {
		frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		mainPanel.setBorder(new EmptyBorder(5, 5, 5, 5));

		setButtons();

		frame.setContentPane(mainPanel);
		frame.pack();
		frame.setVisible(true);
	}

	/**
	 * @param listener
	 *            Registers buttons listeners to this type of listener
	 */
	public void addButtonListener(ActionListener listener) {
		newChat.addActionListener(listener);
		addFriend.addActionListener(listener);
		removeFriend.addActionListener(listener);
		//streamVideo.addActionListener(listener);
		shareFile.addActionListener(listener);
		logOut.addActionListener(listener);
	}

	/**
	 * 
	 * @param title
	 *            String of the title being passed in
	 * @return A JOptionPane string showing the input dialog for the title
	 *         passed once entered
	 */
	public String showEntryDialog(final String title) {
		return JOptionPane.showInputDialog(title);
	}

	/**
	 * @param message
	 *            String of the title being passed in
	 * @return A JOptionPane string showing the input dialog for the message
	 *         passed once entered
	 */
	public void showErrorDialog(final String message) {
		JOptionPane.showMessageDialog(this.frame, message, "ERROR",
				JOptionPane.ERROR_MESSAGE);
	}

	/**
	 * Adds buttons to the JPanel
	 */
	private void setButtons() {
		buttonPanel.add(newChat);
		buttonPanel.add(addFriend);
		buttonPanel.add(removeFriend);
		//buttonPanel.add(streamVideo);
		buttonPanel.add(shareFile);
		buttonPanel.add(logOut);
	}

	/**
	 * Updates the GUI with select information from the model.
	 * Ended up growing to the extent that many observers are watching the model
	 * and the observers must be able to tell if the update concerns them or not.
	 */
	@Override
	public void update(IMessage<IUpdateInfo> message) {
		final IUpdateInfo info = message.getMessage();
		if (info.forView() == -1) {
			final String up = info.getMessage();
			final int ord = info.getType().ordinal();
			if (ord == UpdateType.ERROR.ordinal())
				JOptionPane.showMessageDialog(frame, up, "ERROR",
						JOptionPane.ERROR_MESSAGE);
			else if (ord == UpdateType.POP_UP.ordinal())
				JOptionPane.showMessageDialog(frame, up, "INFO",
						JOptionPane.INFORMATION_MESSAGE);
			else if (ord == UpdateType.ONLINE.ordinal()) {
				this.onlineX.add(up);
				this.offlineX.remove(up);
				this.setTextArea(onlineX, " Online Friends \n ", onlineA);
				this.setTextArea(offlineX, " Offline Friends \n", offlineA);
			} else if (ord == UpdateType.OFFLINE.ordinal()) {
				this.onlineX.remove(up);
				this.offlineX.add(up);
				this.setTextArea(onlineX, " Online Friends \n ", onlineA);
				this.setTextArea(offlineX, " Offline Friends \n", offlineA);
			} else if (ord == UpdateType.AOU.ordinal()) {
				this.othersX.clear();
				this.othersX.add(up);
				this.setTextArea(othersX, " All Users \n ", othersT);
			}
		}
	}

	private void setTextArea(Set<String> toBe, String init, JTextArea area) {
		area.setText(null);
		area.append(init);
		for (String s : toBe)
			area.append(s);
	}

	public void dispose() {
		this.frame.dispose();
	}
}