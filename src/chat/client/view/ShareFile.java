package chat.client.view;

/* In case we need it */
import java.awt.event.ActionListener;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JButton;

import observer.IMessage;
import observer.IObserver;
import observer.IUpdateInfo;

/**
 * Small GUI for file sharing
 * 
 */
public class ShareFile implements IObserver<IUpdateInfo>{

	private final JFrame frame;
	private final JPanel mainPanel;
	private final List<JButton> files;
	private final JButton cancel;
	private final JLabel jLabel;

	
	/**
	 * Providing a List of strings a set of buttons can be created to allow for a variable number of files to be used.
	 * 
	 * */
	public ShareFile(List<String> fn) {
		files = new ArrayList<JButton>();
		for (String f: fn) {
			files.add(new JButton(f));
		}
		
		cancel = new JButton("Cancel");
		frame = new JFrame("Share");
		jLabel = new JLabel("No file is chosen");
		mainPanel = new JPanel(false);
		setFrame();
	}

	/**
	 * @param label The label string which would become the name of the file
	 */
	public void setLabel(String label) {
		jLabel.setText(label);
	}

	/**
	 * @return The JLabel
	 */
	private JLabel getLabel() {
		return jLabel;
	}

	/**
	 * Creates the frame and sets the panel
	 */
	private void setFrame() {
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mainPanel.add(new javax.swing.JLabel("Please select a file to share"));
		for (JButton j: files) mainPanel.add(j);
		mainPanel.add(cancel);
		mainPanel.add(getLabel());
		frame.setContentPane(mainPanel);
		frame.pack();
		frame.setVisible(true);
		frame.setBounds(50, 50, 700, 400);
	}
	
	public void dispose() {
		this.frame.dispose();
	}

	/**
	 * Adds listeners to each button
	 * 
	 * @param listener
	 *            The type of listener being passed in
	 */
	public void addButtonListener(ActionListener listener) {
		cancel.addActionListener(listener);
		for(JButton j: files) j.addActionListener(listener);
	}



	@Override
	public void update(IMessage<IUpdateInfo> msg) {
		setFrame();
		
	}

}
