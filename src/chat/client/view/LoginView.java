package chat.client.view;

import java.awt.GridLayout;
import java.awt.event.ActionListener;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

import observer.IMessage;
import observer.IObserver;
import observer.IUpdateInfo;
import observer.UpdateType;

/**
 * The purpose of this observer class is to create the login view
 */
public class LoginView implements IObserver<IUpdateInfo> {

	private final JFrame frame;
	private final JPanel mainPanel;
	private final JTextField userName, host;
	private final JPasswordField passWord;
	private final JButton submit;
	private final JRadioButton login, register;
	//private final JCheckBox encrypt;

	public LoginView() {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException | InstantiationException
				| IllegalAccessException | UnsupportedLookAndFeelException e) {
		}
		register = new JRadioButton("Register");
		login = new JRadioButton("Login");
		//encrypt = new JCheckBox("Encrypt");
		userName = new JTextField();
		passWord = new JPasswordField();
		mainPanel = new JPanel(new GridLayout(6, 2, 6, 3));
		frame = new JFrame("Join");
		host = new JTextField();
		submit = new JButton("Submit");
		setFrame();
	}

	/**
	 * Sets the frame and its constraints, in addition, adds a non modifiable
	 * JLabels
	 */
	private void setFrame() {
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mainPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		mainPanel.add(new JLabel("Username"));
		mainPanel.add(userName);
		mainPanel.add(new JLabel("Password"));
		mainPanel.add(passWord);
		mainPanel.add(new JLabel("Host"));
		mainPanel.add(host);

		setButtons();

		frame.setContentPane(mainPanel);
		frame.pack();
		frame.setVisible(true);
	}

	/**
	 * Sets the buttons on the view
	 */
	private void setButtons() {
		final ButtonGroup group = new ButtonGroup();

		group.add(login);
		group.add(register);

		mainPanel.add(login);
		mainPanel.add(register);
		//mainPanel.add(encrypt);
		mainPanel.add(submit);
	}

	/**
	 * @param message
	 *            The message to be passed to the view, the message depends on
	 *            what the user have entered etc
	 */
	@Override
	public void update(IMessage<IUpdateInfo> message) {
		final IUpdateInfo info = message.getMessage();
		final int ord = info.getType().ordinal();
		if (ord == UpdateType.ERROR.ordinal())
			JOptionPane.showMessageDialog(frame, info.getMessage(), "ERROR",
					JOptionPane.ERROR_MESSAGE);
		else if (ord == UpdateType.POP_UP.ordinal())
			JOptionPane.showMessageDialog(frame, info.getMessage(), "INFO",
					JOptionPane.INFORMATION_MESSAGE);
	}

	/**
	 * @param message
	 *            The error message
	 */
	public void showError(String message) {
		JOptionPane.showMessageDialog(frame, message, "ERROR",
				JOptionPane.ERROR_MESSAGE);
	}

	/**
	 * @param listener
	 *            Action listener to register each button's listeners
	 */
	public void addButtonListener(ActionListener listener) {
		submit.addActionListener(listener);
	}

	/**
	 * @return The password as a string from the password field
	 */
	public String getPassword() {
		return new String(passWord.getPassword());
	}

	/**
	 * @return Whether or not the chequebooks Encrypt is selected
	 * Note: Check-box and encryption set-up incomplete due to lack of time;
	 * control mechanisms exist and model can still be informed.
	 */
	public boolean isEncrypted() {
		return false;
	}

	/**
	 * @return Whether the register button is selected
	 */
	public boolean isRegister() {
		return this.register.isSelected();
	}

	/**
	 * @return Whether the login button is selected
	 */
	public boolean isLogin() {
		return this.login.isSelected();
	}

	/**
	 * @return The host as a string
	 */
	public String getHost() {
		return host.getText();
	}

	/**
	 * @return The user name entered
	 */
	public String getUserName() {
		return userName.getText();
	}

	/**
	 * Disposes the frame
	 */
	public void dispose() {
		this.frame.dispose();
	}
}