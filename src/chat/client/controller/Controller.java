package chat.client.controller;

import observer.IUpdateInfo;
import chat.client.model.IChatClient;

/**
 * The purpose of this class is to control the overall GUI by switching between login and main views
 */
public class Controller {
	private IController<IUpdateInfo> controller;
	private State current;
	
	public Controller(final IChatClient<IUpdateInfo> client){
		this.controller = new LoginController(this, client);
		this.current = State.LOGIN;
	}
	
	/**
	 * Switches between login or main server choices depending on the state
	 */
	public void switchState(){
		switch(current){
			case LOGIN:	this.controller.cleanUp();
						this.controller = new MainController(this, this.controller.getClient());
						this.current = State.MAIN;
						break;
			case MAIN: 	this.controller.cleanUp();
					 	this.controller = new LoginController(this, this.controller.getClient());
					 	this.current = State.LOGIN;
					 	break;
		}
	}
	
	private static enum State{
		LOGIN,
		MAIN
	}
}
