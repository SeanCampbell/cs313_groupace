package chat.client.controller;

import java.awt.event.ActionListener;

import chat.client.model.IChatClient;

public interface IController<T> extends ActionListener {
	
	/**
	 * @return This method returns am instance of client which needs to be controlled
	 */
	IChatClient<T> getClient();
	
	/**
	 * @modifies Any class implements this
	 * @effect Removes the observers and disposes the frame
	 */
	void cleanUp();
}