package chat.client.controller;

import java.awt.event.ActionEvent;

import observer.IUpdateInfo;

import chat.client.model.IChatClient;
import chat.client.view.LoginView;

/**
 * Controls the login frame, extracts the information that were entered by the
 * user then controls model according to that information
 */
public class LoginController implements IController<IUpdateInfo> {
	private final Controller control; // Instance of the controller
	private final IChatClient<IUpdateInfo> client; // Client
	private LoginView view; // The login view frame which displays everything

	public LoginController(final Controller control,
			final IChatClient<IUpdateInfo> client) {
		this.control = control;
		this.client = client;
		this.view = new LoginView();
		this.client.addObserver(view);
		this.view.addButtonListener(this);
	}

	/**
	 * @effect Once the submit button is clicked, all of the information is
	 *         passed to model
	 */
	@Override
	public void actionPerformed(ActionEvent action) {
		switch (action.getActionCommand()) {
		case "Submit":
			this.submitToClient();
		default:
			break;
		}
	}

	/**
	 * @effect Reads the information that the user have provided in order to get
	 *         access
	 * @exception InterruptedException
	 */
	private void submitToClient() {
		final String pass = this.view.getPassword(), user = this.view
				.getUserName(), host = this.view.getHost();
		final boolean encrypt = this.view.isEncrypted(), toRegister = this.view
				.isRegister(), toLogin = this.view.isLogin();
		// Check for empty text field and whether wish to register or login
		if (pass.hashCode() == 0 || user.hashCode() == 0
				|| host.hashCode() == 0) // If the user did not write anything
			this.view.showError("Invalid submission attempt!  Blank field!");
		else {
			if (!(toRegister ^ toLogin)) // The user should choose either to
											// login or register
				this.view
						.showError("Invalid submission attempt! Select register or login!");
			else {
				if (this.client.connectToServer(host, user, pass, toRegister,
						encrypt)) {
					// Pretend it takes a little bit
					try {
						Thread.sleep(500);
					} catch (InterruptedException e) {
					}
					this.control.switchState();
				}
			}
		}
	}

	/**
	 * @modifies this
	 * @requires client != null
	 * @effect removes and observer and disposes the view
	 */
	@Override
	public void cleanUp() {
		this.client.removeObserver(this.view);
		this.view.dispose();
		this.view = null;
	}

	/**
	 * @return Instance of client
	 */
	@Override
	public IChatClient<IUpdateInfo> getClient() {
		return this.client;
	}
}