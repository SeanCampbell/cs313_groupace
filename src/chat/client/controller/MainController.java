package chat.client.controller;

import java.awt.event.ActionEvent;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.Timer;

import chat.client.model.IChatClient;
import observer.IUpdateInfo;
import chat.client.view.MainView;

/**
 * This class aims to control the initial view which first appears after a
 * client registers/logs on
 */
public class MainController implements IController<IUpdateInfo> {
	// How quick the refresh rate should be.
	private static final int TIMER_DELAY = 500;

	private final Controller control; // Instance of control
	private final IChatClient<IUpdateInfo> client; // a client
	// Map of chat rooms which maps the chat ID and its controller
	private final Map<Integer, IController<IUpdateInfo>> chatRooms;
	private final Timer timer; // Java swing timer
	private MainView view; // The initial view GUI

	public MainController(final Controller control,
			final IChatClient<IUpdateInfo> client) {
		this.control = control;
		this.client = client;
		this.view = new MainView();
		this.view.addButtonListener(this);
		this.client.addObserver(this.view);

		this.chatRooms = new HashMap<>();
		this.timer = new Timer(MainController.TIMER_DELAY, this);
		this.timer.setActionCommand("TIMER");
		this.timer.setCoalesce(true);
		this.requestInitialUpdate();
		this.timer.start();
	}

	/**
	 * Listens to the buttons pressed on the initial view then for each button
	 * pressed a new frame/action begins
	 */
	@Override
	public void actionPerformed(ActionEvent act) {
		String event = act.getActionCommand();
		switch (event) {
		case "TIMER":
			this.timerUpdate();
			break;
		case "New Chat Room": // If chat is requested
			this.timer.stop();
			this.addNewRoom(); // Start new chat
			this.timer.start();
			break;
		case "Add Friend": // If add friend is requested
			this.timer.stop();
			this.addFriend(); // Add friend
			this.timer.start();
			break;

		case "Remove Friend": // If remove friend is requested
			this.timer.stop();
			this.removeFriend(); // Remove friend
			this.timer.start();
			break;
		case "Stream Video":
			new decoupledGiven.VideoView();
			break;
		case "Share File": // If share file is requested
			this.timer.stop();
			this.shareFile(); // S
			this.timer.start();
			break;
		case "Log Out": // If logout was requested
			this.timer.stop();
			this.logOff(); // Logout
			break;
		}
	}

	private void shareFile() {
		// TODO Auto-generated method stub
		try {
			List<String> files = this.client.requestFileTransfer(null);
			@SuppressWarnings("unused")
			FileShareController fc = new FileShareController(files,this.client);
			
		} catch (ClassNotFoundException | IOException e) {
		}
	}
	

	/**
	 * @modifies This
	 * @requires Client != null
	 * @effect Notifies the client with the status of their friends
	 * @exception ClassNotFoundException
	 *                | IOException These exceptions could be caught if there
	 *                was an issue with getting the status of friends list
	 */
	private void requestInitialUpdate() {
		try {
			this.client.requestInitialUpdate();
		} catch (ClassNotFoundException | IOException e) {
			this.view
					.showErrorDialog("THERE WAS A PROBLEM REQUESTING THE STATUS OF YOUR FRIENDS!");
		}
	}

	/**
	 * @requires client != null
	 * @modifies this
	 * @effect Updates the timer with the new timer task
	 */
	private void timerUpdate() {
		try {
			int ret = this.client.update();
			// Ret of -1 means for main view. Ret of Min means terminate. If the
			// map doesn't have view, add it.
			if (ret != -1 && ret != Integer.MIN_VALUE
					&& !this.chatRooms.containsKey(ret))
				this.chatRooms.put(ret, new ChatViewController(ret, this,
						this.client));
			// Check there isn't a back-log. If there is, deal with it
			// appropriately by repeating above.
			while (this.client.arePacketsWaiting()) {
				int inRet = this.client.update();
				if (inRet != -1 && !this.chatRooms.containsKey(inRet))
					this.chatRooms.put(inRet, new ChatViewController(inRet,
							this, this.client));
			}
		} catch (ClassNotFoundException | IOException e) {
		}
	}

	/**
	 * @requires client != null
	 * @modifies this
	 * @effect Add the new chat room if the ID of the new room to be added does
	 *         not exist
	 * @exception ClassNotFoundException
	 *                | IOException
	 */
	private void addNewRoom() {
		try {
			int id = this.client.requestNewChatRoom();
			if (!this.chatRooms.containsKey(id)) {
				this.chatRooms.put(id, new ChatViewController(id, this,
						this.client));
			}
		} catch (ClassNotFoundException | IOException e) {
		}
	}

	/**
	 * @modifies this
	 * @requires client != null
	 * @effect Adds friend to the friend's list of the chosen client
	 * @exception ClassNotFoundException
	 *                | IOException
	 */
	private void addFriend() {
		String addFriend = this.view.showEntryDialog("Friend to add:");
		try {
			this.client.addFriend(addFriend);
		} catch (ClassNotFoundException | IOException e) {
		}
	}

	/**
	 * @modifies this
	 * @requires client != null
	 * @effect Removes a friend from the friend's list by taking the name from
	 *         the field
	 * @exception ClassNotFoundException
	 *                | IOException
	 */
	private void removeFriend() {
		String removeFriend = this.view.showEntryDialog("Friend to remove:");
		try {
			this.client.removeFriend(removeFriend);
		} catch (ClassNotFoundException | IOException e) {
		}
	}

	/**
	 * @return This client
	 */
	@Override
	public IChatClient<IUpdateInfo> getClient() {
		return this.client;
	}

	/**
	 * @modifies this
	 * @effect removes the room from the map by its ID
	 * @param room
	 *            The ID of the room
	 */
	public void removeRoom(int room) {
		if (this.chatRooms.containsKey(room)) {
			IController<IUpdateInfo> ref = this.chatRooms.remove(room);
			ref.cleanUp();
			ref = null;
		}
	}

	/**
	 * @modifies this
	 * @effect Removes the client from the room, then logs off the client
	 * @exception ClassNotFoundException
	 *                | IOException
	 */
	private void logOff() {
		for (Map.Entry<Integer, IController<IUpdateInfo>> k : this.chatRooms
				.entrySet()) {
			try {
				this.client.leaveRoom(k.getKey());
				k.getValue().cleanUp();
			} catch (ClassNotFoundException | IOException e) {
			}
		}
		this.chatRooms.clear();
		try {
			this.client.logOff();
		} catch (ClassNotFoundException | IOException e) {
		}
		this.control.switchState();
	}

	/**
	 * @modifies this
	 * @effect removes an observer and disposes the view frame then disconnects
	 *         the client
	 * @exception IOException
	 */
	@Override
	public void cleanUp() {
		this.client.removeObserver(this.view);
		this.view.dispose();
		try {
			this.client.disconnect();
		} catch (IOException e) {
		}
	}

}