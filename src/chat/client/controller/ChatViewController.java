package chat.client.controller;

import java.awt.event.ActionEvent;
import java.io.IOException;

import chat.client.model.IChatClient;
import chat.client.view.ChatView;
import observer.IUpdateInfo;

/**
 * Controls the chat view GUI which is controlled by the user, passes
 * information from it to model
 */
public class ChatViewController implements IController<IUpdateInfo> {
	private final MainController owner; // Main GUIs controller
	private final IChatClient<IUpdateInfo> client; // Instance of client
	private final ChatView view; // Instance of the view frame which displays
									// the content

	public ChatViewController(final int id, final MainController owner,
			final IChatClient<IUpdateInfo> client) {
		this.owner = owner;
		this.client = client;
		this.view = new ChatView(id);

		this.view.addButtonListener(this);
		this.client.addObserver(this.view);
	}

	/**
	 * Switches between actions that were used then reacts appropriately
	 */
	@Override
	public void actionPerformed(ActionEvent act) {
		final String type = act.getActionCommand();
		switch (type) {
		case "Send":
			this.sendMessage();
			break;
		case "Add":
			this.addPerson();
			break;
		case "Leave":
			this.leaveRoom();
			break;
		}
	}

	/**
	 * @requires client != null
	 * @modifies this
	 * @effect Appends a message to the output text area then clears the input
	 *         text area
	 * @exception ClassNotFoundException
	 *                | IOException
	 */
	private void sendMessage() {
		try {
			final String message = this.view.getInput();
			if (message.hashCode() == 0) // If no text has been written display
											// error
				this.view.showErrorDialog("NO TEXT HAS BEEN ENTERED TO SEND!");
			else
				this.client.sendChatMessage(this.view.getID(), message);
		} catch (ClassNotFoundException | IOException e) {
		}
		this.view.clearInputText();
	}

	/**
	 * @requires client != null
	 * @modifies this
	 * @effect Adds a friend to the friends list of the client, it takes the
	 *         name of the friend to be added from the java default text field
	 *         frame
	 * @exception ClassNotFoundException
	 *                | IOException
	 */
	private void addPerson() {
		final String toAdd = this.view
				.showEntryDialog("Please enter the name: ");
		if (toAdd.hashCode() != 0) {
			try {
				this.client.addToRoom(this.view.getID(), toAdd);
			} catch (ClassNotFoundException | IOException e) {
			}
		}
	}

	/**
	 * @requires client != null
	 * @modifies this
	 * @effect If the client wishes to leave the room it asks whether or not
	 *         they are sure then takes the ID of THIS client and passes it to
	 *         model to exit the client from the room
	 * @exception ClassNotFoundException
	 *                | IOException
	 */
	private void leaveRoom() {
		final boolean sure = this.view
				.showConfirmDialog("Are you sure?");
		if (sure)
			try {
				final int id = this.view.getID();
				if (this.client.leaveRoom(id))
					this.owner.removeRoom(id);
			} catch (ClassNotFoundException | IOException e) {
			}
	}

	/**
	 * @return Instance of this client
	 */
	@Override
	public IChatClient<IUpdateInfo> getClient() {
		return this.client;
	}

	/**
	 * @requires client != null
	 * @modifies this
	 * @effect disposes the view and removes the observer
	 */
	@Override
	public void cleanUp() {
		this.client.removeObserver(this.view);
		this.view.dispose();
	}

}
