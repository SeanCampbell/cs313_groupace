package chat.client.controller;



import java.util.List;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;

import javax.swing.JFileChooser;

import observer.IUpdateInfo;
import chat.client.model.IChatClient;
import chat.client.view.ShareFile;

/**
 * This class controls the view of the share file
 * 
 * This was developed in its framework by Mustafa Hashem and 
 * expanded to the final product by William McCusker* 
 * 
 */
public class FileShareController implements IController<IUpdateInfo> {

	private ShareFile sf;
	private IChatClient<IUpdateInfo> c;

	public FileShareController(List<String> files, IChatClient<IUpdateInfo> c) {
		this.sf = new ShareFile(files);
		this.sf.addButtonListener(this);
		this.c = c;
		this.c.addObserver(this.sf);
		
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		String n;
		switch (e.getActionCommand()) {
		case "Cancel":
			cleanUp();
			break;
		default:
			n = e.getActionCommand();
			try {
				this.c.requestFileTransfer(n);
			} catch (ClassNotFoundException | IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			break;
		}
	}

	@SuppressWarnings("unused")
	private File getFile() {
		JFileChooser fc = new JFileChooser();
		int returnVal = fc.showOpenDialog(null);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			File file = fc.getSelectedFile();
			return file = new File(file.getPath());
		}
		return null;
	}

	@Override
	public IChatClient<IUpdateInfo> getClient() {
		return null;
	}

	@Override
	public void cleanUp() {
		this.c.removeObserver(this.sf);
		this.sf.dispose();
		
	}

}