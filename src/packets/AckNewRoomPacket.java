package packets;

import chat.server.packetHandling.IClientPacketHandler;
import chat.server.packetHandling.IServerPacketHandler;

public class AckNewRoomPacket implements IPacket<Integer> {

	private static final long serialVersionUID = 7013516675467108074L;

	private final PacketType type;
	
	private final int info;
	private final long timeStamp;
	
	public AckNewRoomPacket(int info) {
		this.type = PacketType.ACK_NEW_ROOM;
		this.info = info;
		this.timeStamp = System.currentTimeMillis();
	}
	
	@Override
	public PacketType getType() {
		return this.type;
	}

	@Override
	public boolean isEncrypted() {
		return false;
	}

	@Override
	public long getTimeStamp() {
		return this.timeStamp;
	}

	@Override
	public Integer getMessage() {
		return this.info;
	}

	@Override
	public int getExpectedLength() {
		return 1;
	}

	@Override
	public void accept(IServerPacketHandler handler) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void accept(IClientPacketHandler handler) {
		handler.visitProcessPacket(this);
	}
}
