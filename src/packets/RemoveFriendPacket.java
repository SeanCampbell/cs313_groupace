package packets;

import chat.server.packetHandling.IClientPacketHandler;
import chat.server.packetHandling.IServerPacketHandler;

public class RemoveFriendPacket implements IPacket<String>{

	private static final long serialVersionUID = -9097268322143195414L;

	private final PacketType type;
	private final String info;
	
	private final long timeStamp;
	
	public RemoveFriendPacket(String friend) {
		this.type = PacketType.REMOVE_FRIEND;
		this.info = friend;
		this.timeStamp = System.currentTimeMillis();
	}

	@Override
	public PacketType getType() {
		return this.type;
	}

	@Override
	public boolean isEncrypted() {
		return false;
	}

	@Override
	public long getTimeStamp() {
		return this.timeStamp;
	}

	@Override
	public String getMessage() {
		return this.info;
	}	
	
	@Override
	public int getExpectedLength() {
		return 1;
	}

	@Override
	public void accept(IServerPacketHandler handler) {
		handler.visitProcessPacket(this);
	}

	@Override
	public void accept(IClientPacketHandler handler) {
		throw new UnsupportedOperationException();
	}
}
