package packets;

import chat.server.packetHandling.IClientPacketHandler;
import chat.server.packetHandling.IServerPacketHandler;


public class AckPacket extends Packet {

	private static final long serialVersionUID = 4337323437149041268L;
	
	private boolean suppress;
	
	public AckPacket(String message) {
		super(PacketType.ACNOWLEDGE, false, message);
		this.suppress = false;
	}
	
	public AckPacket(String message, boolean suppress) {
		super(PacketType.ACNOWLEDGE, false, message);
		this.suppress = suppress;
	}
	
	@Override
	public void accept(IServerPacketHandler handler) {
		throw new UnsupportedOperationException();
	}
	
	@Override
	public void accept(IClientPacketHandler handler) {
		 handler.visitProcessPacket(this);
	}
	
	public boolean canSuppress(){
		return this.suppress;
	}
}
