package packets;

import chat.server.packetHandling.IClientPacketHandler;
import chat.server.packetHandling.IServerPacketHandler;

public class ErrorPacket extends Packet {

	private static final long serialVersionUID = -8389665239532316384L;

	public ErrorPacket(String message) {
		super(PacketType.ERROR, false, message);
	}

	@Override
	public int getExpectedLength() {
		// Error size is variable
		return super.getMessage().size();
	}

	@Override
	public void accept(IServerPacketHandler handler) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void accept(IClientPacketHandler handler) {
		handler.visitProcessPacket(this);
	}
}
