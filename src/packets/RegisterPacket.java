package packets;

import chat.server.packetHandling.IClientPacketHandler;
import chat.server.packetHandling.IServerPacketHandler;

public class RegisterPacket extends Packet {

	private static final long serialVersionUID = 7203484286155736354L;

	public RegisterPacket(final boolean encrypt, final String message) {
		super(PacketType.REGISTER, encrypt, message);
	}
	
	@Override
	public int getExpectedLength(){
		return 2;
	}

	@Override
	public void accept(IServerPacketHandler handler) {
		handler.visitProcessPacket(this);
	}

	@Override
	public void accept(IClientPacketHandler handler) {
		throw new UnsupportedOperationException();
	}
}