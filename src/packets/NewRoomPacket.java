package packets;

import chat.server.packetHandling.IClientPacketHandler;
import chat.server.packetHandling.IServerPacketHandler;

public class NewRoomPacket implements IPacket<Void>{

	private static final long serialVersionUID = 3028372122904498389L;

	private final PacketType type;
	
	private final long timeStamp;
	
	public NewRoomPacket() {
		this.type = PacketType.NEW_ROOM;
		this.timeStamp = System.currentTimeMillis();
	}

	@Override
	public PacketType getType() {
		return this.type;
	}

	@Override
	public boolean isEncrypted() {
		return false;
	}

	@Override
	public long getTimeStamp() {
		return this.timeStamp;
	}

	@Override
	public Void getMessage() {
		return null;
	}
	
	@Override
	public int getExpectedLength() {
		return 0;
	}

	@Override
	public void accept(IServerPacketHandler handler) {
		handler.visitProcessPacket(this);
	}

	@Override
	public void accept(IClientPacketHandler handler) {
		throw new UnsupportedOperationException();
	}
}
