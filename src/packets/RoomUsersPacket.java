package packets;

import chat.server.packetHandling.IClientPacketHandler;
import chat.server.packetHandling.IServerPacketHandler;

public class RoomUsersPacket implements IPacket<String> {

	private static final long serialVersionUID = -6172859659461646507L;

	private final PacketType type;
	
	private final String info, name;
	private final long timeStamp;
	
	private final int room;
	
	public RoomUsersPacket(int room, String message, String name){
		this.type = PacketType.ROOM_USERS;
		
		this.room = room;
		this.info = message;
		this.name = name;
		
		this.timeStamp = System.currentTimeMillis();
	}
	
	@Override
	public PacketType getType() {
		return this.type;
	}

	@Override
	public boolean isEncrypted() {
		return false;
	}

	@Override
	public long getTimeStamp() {
		return this.timeStamp;
	}

	@Override
	public int getExpectedLength() {
		return this.info.length();
	}

	@Override
	public String getMessage() {
		return this.info;
	}
	
	@Override
	public void accept(IServerPacketHandler handler) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void accept(IClientPacketHandler handler) {
		handler.visitProcessPacket(this);
	}
	
	public int getRoom(){
		return this.room;
	}
	
	public String getName(){
		return this.name;
	}

}
