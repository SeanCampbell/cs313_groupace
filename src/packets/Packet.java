package packets;

import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public abstract class Packet implements IPacket<List<String>> {
	private transient static final long serialVersionUID = -6323128419335805295L;
	
	public static final Charset ENCODING = Charset.forName("UTF-8");
	public static final String DELIM = new String(Packet.ENCODING.encode("/n").array()).toString();
	
	private final PacketType type;
	
	private final List<String> info;
	
	private final boolean isEncrypted;
	private final long timeStamp;
	
	public Packet(final PacketType type, final boolean isEncrypted, final String message){
		this.type = type;
		this.isEncrypted = isEncrypted;
		this.info = Arrays.asList(message.split(Packet.DELIM));
		this.timeStamp = System.currentTimeMillis();
	}
	
	public Packet(final PacketType type, final boolean isEncrypted, final List<String> list){
		this.type = type;
		this.isEncrypted = isEncrypted;
		this.info = list;
		this.timeStamp = System.currentTimeMillis();
	}
	
	@Override
	public PacketType getType() {
		return this.type;
	}
	
	@Override
	public boolean isEncrypted(){
		return this.isEncrypted;
	}
	
	@Override
	public long getTimeStamp() {
		return this.timeStamp;
	}
	
	@Override
	public List<String> getMessage(){
		return Collections.unmodifiableList(this.info);
	}
	
	@Override
	public int getExpectedLength(){
		return this.info.size();
	}
}
