package packets;

import chat.server.packetHandling.IClientPacketHandler;
import chat.server.packetHandling.IServerPacketHandler;

public class AddFriendPacket implements IPacket<String> {

	private static final long serialVersionUID = 502935868187386275L;
	private final PacketType type;
	
	private final String info;

	private final long timeStamp;
	
	public AddFriendPacket(String friend) {
		this.type = PacketType.ADD_FRIEND;
		this.info = friend;
		this.timeStamp = System.currentTimeMillis();
	}

	@Override
	public PacketType getType() {
		return this.type;
	}

	@Override
	public boolean isEncrypted() {
		return false;
	}

	@Override
	public long getTimeStamp() {
		return this.timeStamp;
	}

	@Override
	public String getMessage() {
		return this.info;
	}	
	
	@Override
	public int getExpectedLength() {
		return 1;
	}

	@Override
	public void accept(IServerPacketHandler handler) {
		handler.visitProcessPacket(this);
	}

	@Override
	public void accept(IClientPacketHandler handler) {
		throw new UnsupportedOperationException();
	}
}
