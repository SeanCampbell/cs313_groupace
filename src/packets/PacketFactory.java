package packets;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PacketFactory {
	private static final String PACKAGE = PacketFactory.class.getPackage().getName();
	
	private final Map<PacketType, Class<? extends IPacket<?>>> registeredPackets;
	
	static{
		try{
			Class.forName(PacketFactory.PACKAGE + ".RegisterPacket");
			Class.forName(PacketFactory.PACKAGE + ".LoginPacket");
			
		} catch(ClassNotFoundException e){
			e.printStackTrace();
			System.exit(-1);
		}
	}
	
	public PacketFactory(){
		this.registeredPackets = new HashMap<>();
	}
	
	public boolean canCreate(PacketType type){
		return this.registeredPackets.containsKey(type);
	}
	
	public void addToFactory(PacketType type, Class<? extends IPacket<?>> value){
		this.registeredPackets.put(type, value);
	}
	
	public IPacket<?> createPacket(PacketType type) throws InstantiationException, IllegalAccessException{
		if(!this.registeredPackets.containsKey(type))
			throw new InstantiationException("Unrecognised PacketType:" + type.toString());
		return this.registeredPackets.get(type).newInstance();
	}
	
	public IPacket<?> createPacket(PacketType type, Object...args) throws InstantiationException, IllegalAccessException,
		IllegalArgumentException, InvocationTargetException{
		if(!this.registeredPackets.containsKey(type))
			throw new InstantiationException("Unrecognised PacketType:" + type.toString());	
		Constructor<? extends IPacket<?>> ctor = this.getConstructor(type, args);
		return ctor.newInstance(args);
	}
	
	
	private Constructor<? extends IPacket<?>> getConstructor(PacketType name, Object...args) throws InstantiationException{
		@SuppressWarnings("unchecked")
		Constructor<? extends IPacket<?>>[] ctors = (Constructor<? extends IPacket<?>>[])this.registeredPackets.get(name).getConstructors();
		
		for(Constructor<? extends IPacket<?>> ctor : ctors){
			if(ctor.getParameterTypes().length == args.length){
				boolean possibleMatch = true;
				
				// Fetch all the classes corresponding to this constructor
				List<Class<?>> classes = new ArrayList<>(Arrays.asList(ctor.getParameterTypes()));
				
				for(int i = 0; i < args.length; i++){
					Class<?> type = args[i].getClass();
					Class<?> ctor_type = classes.get(i);
					
					if(!ctor_type.isAssignableFrom(type)){
						if(ctor_type.isPrimitive()) {
							possibleMatch = (int.class.equals(ctor_type) && Integer.class.equals(type))
									|| (double.class.equals(ctor_type)) && Double.class.equals(type)
				                    || (long.class.equals(ctor_type) && Long.class.equals(type))
				                    || (char.class.equals(ctor_type) && Character.class.equals(type))
				                    || (short.class.equals(ctor_type) && Short.class.equals(type))
				                    || (boolean.class.equals(ctor_type) && Boolean.class.equals(type))
				                    || (byte.class.equals(ctor_type) && Byte.class.equals(type));
						}
						else if(ctor_type.equals(type))
							possibleMatch = true;
						else {
							possibleMatch = false;
							break;
						}
					}
				}
				if(possibleMatch)
					return ctor;
			}
		}
		throw new InstantiationException("Could not find acceptable constructor");
	}

}

