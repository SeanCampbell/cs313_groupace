package packets;

import chat.server.packetHandling.IClientPacketHandler;
import chat.server.packetHandling.IServerPacketHandler;

public class AllOnlineUsersPacket implements IPacket<String> {
	
	private static final long serialVersionUID = 2693696429721763399L;

	private final PacketType type;

	private final String info;

	private final long timeStamp;

	public AllOnlineUsersPacket(String friend) {
		this.type = PacketType.ALL_ONLINE_USERS;
		this.info = friend;
		this.timeStamp = System.currentTimeMillis();
	}

	@Override
	public PacketType getType() {
		return this.type;
	}

	@Override
	public boolean isEncrypted() {
		return false;
	}

	@Override
	public long getTimeStamp() {
		return this.timeStamp;
	}

	@Override
	public String getMessage() {
		return this.info;
	}

	@Override
	public int getExpectedLength() {
		return this.info.length();
	}

	@Override
	public void accept(IServerPacketHandler handler) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void accept(IClientPacketHandler handler) {
		handler.visitProcessPacket(this);
	}
}
