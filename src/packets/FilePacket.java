package packets;

import java.util.Collections;
import java.util.List;

import chat.server.packetHandling.IClientPacketHandler;
import chat.server.packetHandling.IServerPacketHandler;

public class FilePacket implements IPacket<List<Integer>>{

	private static final long serialVersionUID = 550903382374104468L;
	private final PacketType type;
	private final List<Integer> contents;
	private final String fileName;
	private final long timeStamp;
	
	public FilePacket(final List<Integer> contents, final String fileName) {
		this.type = PacketType.FILE;
		this.contents = contents;
		this.fileName = fileName;
		this.timeStamp = System.currentTimeMillis();
	}
	
	@Override
	public PacketType getType() {
		return this.type;
	}

	@Override
	public boolean isEncrypted() {
		return false;
	}

	@Override
	public long getTimeStamp() {
		return this.timeStamp;
	}

	@Override
	public int getExpectedLength() {
		return this.contents.size();
	}

	@Override
	public List<Integer> getMessage() {
		return Collections.unmodifiableList(this.contents);
	}

	@Override
	public void accept(IServerPacketHandler handler) {
		throw new UnsupportedOperationException();
		
	}

	@Override
	public void accept(IClientPacketHandler handler) {
		handler.visitProcessPacket(this);
		
	}
	
	public String getFileName() {
		return fileName;
	}

}
