package packets;

import chat.server.packetHandling.IClientPacketHandler;
import chat.server.packetHandling.IServerPacketHandler;

public class NewMessagePacket implements IPacket<String> {

	private static final long serialVersionUID = -5568952146787140027L;
	
	private final PacketType type;
	
	private final String info;
	private final long timeStamp;
	
	private final int room;
	private final String sender;
	
	public NewMessagePacket(int room, String sender, String message){
		this.type = PacketType.NEW_MESSAGE;
		
		this.room = room;
		this.sender = sender;
		this.info = message;
		
		this.timeStamp = System.currentTimeMillis();
	}
	
	@Override
	public PacketType getType() {
		return this.type;
	}

	@Override
	public boolean isEncrypted() {
		return false;
	}

	@Override
	public long getTimeStamp() {
		return this.timeStamp;
	}

	@Override
	public int getExpectedLength() {
		return this.info.length();
	}

	@Override
	public String getMessage() {
		return this.info;
	}
	
	@Override
	public void accept(IServerPacketHandler handler) {
		handler.visitProcessPacket(this);
	}

	@Override
	public void accept(IClientPacketHandler handler) {
		handler.visitProcessPacket(this);
	}
	
	/**
	 * 
	 * @return the room
	 */
	public int getRoom(){
		return this.room;
	}
	
	/**
	 * 
	 * @return the sender
	 */
	public String getSender(){
		return this.sender;
	}
}
