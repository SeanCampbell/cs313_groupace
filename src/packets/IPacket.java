package packets;

import java.io.Serializable;

import chat.server.packetHandling.IClientPacketHandler;
import chat.server.packetHandling.IServerPacketHandler;

public interface IPacket<T> extends Serializable {
	/**
	 * 
	 * @return the type
	 */
	PacketType getType();
	
	/**
	 * 
	 * @return if the packet is encrypted or not
	 */
	boolean isEncrypted();

	/**
	 * 
	 * @return the time stamp
	 */
	long getTimeStamp();
	
	/**
	 * 
	 * @return the expected length of the contents of a packet
	 */
	int getExpectedLength();
	
	/**
	 * 
	 * @return the message in the packet
	 */
	T getMessage();
	
	// Visitor pattern, pass up to handlers
	void accept(IServerPacketHandler handler);
	void accept(IClientPacketHandler handler);
}