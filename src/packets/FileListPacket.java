package packets;

import java.util.Collections;
import java.util.List;

import chat.server.packetHandling.IClientPacketHandler;
import chat.server.packetHandling.IServerPacketHandler;

public class FileListPacket implements IPacket<List<String>>{


	private static final long serialVersionUID = 3798418143894593343L;
	private final PacketType type;
	private final List<String> contents;
	private final long timeStamp;
	
	public FileListPacket(final List<String> contents) {
		this.type = PacketType.FILELIST;
		this.contents = contents;
		this.timeStamp = System.currentTimeMillis();
	}
	
	@Override
	public PacketType getType() {
		return this.type;
	}

	@Override
	public boolean isEncrypted() {
		return false;
	}

	@Override
	public long getTimeStamp() {
		return this.timeStamp;
	}

	@Override
	public int getExpectedLength() {
		return this.contents.size();
	}

	@Override
	public List<String> getMessage() {
		return Collections.unmodifiableList(this.contents);
	}

	@Override
	public void accept(IServerPacketHandler handler) {

		throw new UnsupportedOperationException();
		
	}

	@Override
	public void accept(IClientPacketHandler handler) {
		handler.visitProcessPacket(this);
		
	}
	


}
