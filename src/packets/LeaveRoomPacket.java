package packets;

import chat.server.packetHandling.IClientPacketHandler;
import chat.server.packetHandling.IServerPacketHandler;

public class LeaveRoomPacket implements IPacket<Integer> {

	private static final long serialVersionUID = -3804634492825342719L;

	private final PacketType type;
	
	private final int info;
	private final long timeStamp;
	
	public LeaveRoomPacket(int room) {
		this.type = PacketType.LEAVE_ROOM;
		this.timeStamp = System.currentTimeMillis();
		this.info = room;
	}

	@Override
	public PacketType getType() {
		return this.type;
	}

	@Override
	public boolean isEncrypted() {
		return false;
	}

	@Override
	public long getTimeStamp() {
		return this.timeStamp;
	}

	@Override
	public Integer getMessage() {
		return this.info;
	}	
	
	@Override
	public int getExpectedLength() {
		return 1;
	}

	@Override
	public void accept(IServerPacketHandler handler) {
		handler.visitProcessPacket(this);
	}

	@Override
	public void accept(IClientPacketHandler handler) {
		//handler.visitProcessPacket(this);
	}
}
