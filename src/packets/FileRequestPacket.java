package packets;

import chat.server.packetHandling.IClientPacketHandler;
import chat.server.packetHandling.IServerPacketHandler;

public class FileRequestPacket implements IPacket<String>{

	private static final long serialVersionUID = -4113571041232566027L;
	private final PacketType type;
	private final long timeStamp;
	private final String name;
	
	public FileRequestPacket(String name) {
		this.name = name;
		this.type = PacketType.FILEREQ;
		this.timeStamp = System.currentTimeMillis();
	}
	
	@Override
	public PacketType getType() {
		return this.type;
	}

	@Override
	public boolean isEncrypted() {
		return false;
	}

	@Override
	public long getTimeStamp() {
		return this.timeStamp;
	}

	@Override
	public int getExpectedLength() {
		return 1;
	}

	@Override
	public String getMessage() {
		return name;
	}

	@Override
	public void accept(IServerPacketHandler handler) {
		handler.visitProcessPacket(this);
		
	}

	@Override
	public void accept(IClientPacketHandler handler) {
		throw new UnsupportedOperationException();
		
	}
	
	

}
