package packets;

import chat.server.packetHandling.IClientPacketHandler;
import chat.server.packetHandling.IServerPacketHandler;

public class AddToRoomPacket implements IPacket<String> {
	
	private static final long serialVersionUID = -4546478486743425292L;

	private final PacketType type;
	
	private final String info;
	private final long timeStamp;
	
	private final int room;
	
	public AddToRoomPacket(int room, String friend) {
		this.type = PacketType.ADD_TO_ROOM;
		this.info = friend;
		this.timeStamp = System.currentTimeMillis();
		this.room = room;
	}

	@Override
	public PacketType getType() {
		return this.type;
	}

	@Override
	public boolean isEncrypted() {
		return false;
	}

	@Override
	public long getTimeStamp() {
		return this.timeStamp;
	}

	@Override
	public String getMessage() {
		return this.info;
	}	
	
	@Override
	public int getExpectedLength() {
		return this.info.length();
	}

	@Override
	public void accept(IServerPacketHandler handler) {
		handler.visitProcessPacket(this);
	}

	@Override
	public void accept(IClientPacketHandler handler) {
		handler.visitProcessPacket(this);
	}
	
	public int getRoom(){
		return this.room;
	}
}
