package packets;

import chat.server.packetHandling.IClientPacketHandler;
import chat.server.packetHandling.IServerPacketHandler;

public class AckPeopleNotificationPacket implements IPacket<String> {

	private static final long serialVersionUID = 6564216349830308949L;

	private final PacketType type;
	
	private final String online, offline;
	private final long timeStamp;
	
	public AckPeopleNotificationPacket(String online, String offline){
		this.type = PacketType.USER_FRIENDS;
		
		this.online = online;
		this.offline = offline;
		
		this.timeStamp = System.currentTimeMillis();
	}
	
	@Override
	public PacketType getType() {
		return this.type;
	}

	@Override
	public long getTimeStamp() {
		return this.timeStamp;
	}

	@Override
	public int getExpectedLength() {
		return 2;
	}
	// One could simply use this and delimit on a known character but since the packet handler
	// knows the type of packet, it is no longer required to work with an interface
	@Override
	public String getMessage() {
		return this.online + "|\n" + this.offline;
	}
	
	@Override
	public void accept(IServerPacketHandler handler) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void accept(IClientPacketHandler handler) {
		handler.visitProcessPacket(this);
	}
	
	public String getOnline(){
		return this.online;
	}
	
	public String getOffline(){
		return this.offline;
	}

	@Override
	public boolean isEncrypted() {
		return false;
	}
}
