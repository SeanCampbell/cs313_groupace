package packets;

import chat.server.packetHandling.IClientPacketHandler;
import chat.server.packetHandling.IServerPacketHandler;

public class LoginPacket extends Packet {

	private static final long serialVersionUID = 3428648442051113693L;

	public LoginPacket(final boolean encrypt, final String message) {
		super(PacketType.LOGIN, encrypt, message);
	}
	
	@Override
	public int getExpectedLength(){
		return 2;
	}

	@Override
	public void accept(IServerPacketHandler handler) {
		handler.visitProcessPacket(this);
	}

	@Override
	public void accept(IClientPacketHandler handler) {
		throw new UnsupportedOperationException();
	}
}
